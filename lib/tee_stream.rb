# frozen_string_literal: true
require 'open3'
require 'stringio'

module TeeStream
  Result = Struct.new(:status, :stdout, :stderr)

  module_function

  def tee_stream(input, *outputs)
    Thread.new do
      until input.eof?
        input.wait_readable
        string = input.read_nonblock(8192, exception: false)
        outputs.each do |output|
          output.write(string)
          output.flush
        end

        yield(string) if block_given?
      end
    end
  end

  def exec(cmd, out: $stdout, err: $stderr)
    raise ArgumentError, 'System command must be an array of strings' unless cmd.is_a?(Array)

    Open3.popen3(*cmd) do |stdin, cmd_stdout, cmd_stderr, wait_thr|
      stdin.close

      out_buffer = StringIO.new
      err_buffer = StringIO.new

      out_thr = tee_stream(cmd_stdout, out, out_buffer)
      err_thr = tee_stream(cmd_stderr, err, err_buffer)

      status = wait_thr.value
      out_thr.join
      err_thr.join

      Result.new(status, out_buffer, err_buffer)
    end
  end
end
