# frozen_string_literal: true

require 'cgi'

class LazyHeatMap
  PRIORITY_LABELS = %w[priority::1 priority::2 priority::3 priority::4].freeze
  SEVERITY_LABELS = %w[severity::1 severity::2 severity::3 severity::4].freeze
  WITHOUT_PRIORITY_STRING = 'No priority'
  WITHOUT_SEVERITY_STRING = 'No severity'
  PRIORITY_TITLES = [*PRIORITY_LABELS, WITHOUT_PRIORITY_STRING].freeze
  SEVERITY_TITLES = [*SEVERITY_LABELS, WITHOUT_SEVERITY_STRING].freeze

  LABELS = {
    priority: {
      pattern: /\Apriority::\d\z/,
      none: WITHOUT_PRIORITY_STRING,
      titles: PRIORITY_TITLES
    },
    severity: {
      pattern: /\Aseverity::\d\z/,
      none: WITHOUT_SEVERITY_STRING,
      titles: SEVERITY_TITLES
    }
  }.freeze

  def initialize(resources, policy_spec, network)
    @resources = resources
    @policy_spec = policy_spec
    @network = network
    @options = network.options
  end

  def to_s
    @to_s ||= generate_heat_map_table
  end

  # https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage/-/issues/255, we wouldn't need to pass options.
  def generate_heat_map_table(options = {})
    body = heat_map.each_key.map do |priority|
      row = [header_string(priority)] +
        SEVERITY_TITLES.map do |severity|
          count = heat_map.dig(priority, severity)&.size || 0
          issues_link(count, issues_query(priority, severity), options)
        end

      "| #{row.join(' | ')} |"
    end.join("\n")

    header = "|| #{SEVERITY_TITLES.map { |s| header_string(s) }.join(' | ')} |"
    separator = "|----|#{SEVERITY_TITLES.map { '----' }.join('|')}|"

    "#{header}\n#{separator}\n#{body}"
  end

  def generate_heat_map_table_stuck(options = {})
    no_assignees      = @resources.select { |resource| resource[:assignees].empty? }
    no_milestone      = @resources.select { |resource| resource[:milestone].nil? }
    milestone_backlog = @resources.select { |resource| resource.dig(:milestone, :title) == 'Backlog' }
    milestone_expired = @resources.select { |resource| resource.dig(:milestone, :expired) }

    data = {
      'No assignees' => {
        issues: group_by_label(no_assignees, :severity),
        filter: { filter_key: 'assignee_id', filter_value: 'None' }
      },
      'No milestone' => {
        issues: group_by_label(no_milestone, :severity),
        filter: { filter_key: 'milestone_title', filter_value: 'None' }
      },
      'Milestone: Backlog' => {
        issues: group_by_label(milestone_backlog, :severity),
        filter: { filter_key: 'milestone_title', filter_value: 'Backlog' }
      },
      'Milestone in the past' => {
        issues: group_by_label(milestone_expired, :severity),
        filter: { filter_key: 'not[milestone_title]', filter_value: 'Upcoming' }
      }
    }

    body = data.map do |row_title, row_hash|
      row = [row_title] +
        row_hash[:issues].map do |severity, issues|
          count = issues&.size || 0
          issues_link(count, issues_query_with_severity(severity, **row_hash[:filter]), options)
        end

      "| #{row.join(' | ')} |"
    end.join("\n")

    header     = "|| #{SEVERITY_TITLES.map { |s| header_string(s) }.join(' | ')} |"
    separator  = "|----|#{SEVERITY_TITLES.map { '----' }.join('|')}|"

    "#{header}\n#{separator}\n#{body}"
  end

  private

  def heat_map
    @heat_map ||= generate_heat_map
  end

  def generate_heat_map
    group_by_label(@resources, :priority).transform_values do |with_same_priority|
      group_by_label(with_same_priority, :severity)
    end
  end

  def pad_and_sort(hash, labels)
    labels.each do |name|
      hash[name] ||= {}
    end

    hash.sort_by { |k, _| k[/\d/] || 'Z' }.to_h
  end

  def group_by_label(resources, label)
    details = LABELS[label]
    grouped_resources = resources.group_by { |resource| resource[:labels].grep(details[:pattern]).min || details[:none] }
    pad_and_sort(grouped_resources, details[:titles])
  end

  def issues_link(count, issues_query, options = {})
    if count.zero?
      count
    else
      <<~MARKDOWN.chomp
        [#{count}](#{issues_base_url(options)}?#{issues_query})
      MARKDOWN
    end
  end

  def issues_base_url(options = {})
    return if @resources.empty?

    # https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage/-/issues/255, we could use
    # `@policy_spec.dig(:limits, :project)` instead of passing options.
    source_url_for_resource(@resources.first, project_id: options[:project_id])
  end

  def issues_query(priority, severity)
    conditions = {
      'state' => issues_states,
      'label_name[]' => issues_label_names + with_sp_labels(priority, severity),
      'not[label_name][]' => issues_forbidden_label_names + without_sp_labels(priority, severity)
    }

    escape_issues_conditions(conditions)
  end

  def issues_query_with_severity(severity, filter_key:, filter_value:)
    conditions = {
      'state' => issues_states,
      'label_name[]' => issues_label_names + with_sp_labels(nil, severity),
      'not[label_name][]' => issues_forbidden_label_names,
      filter_key => [filter_value]
    }

    escape_issues_conditions(conditions)
  end

  def issues_states
    [@policy_spec.dig(:conditions, :state)].compact
  end

  def issues_label_names
    @policy_spec.dig(:conditions, :labels) || []
  end

  def issues_forbidden_label_names
    @policy_spec.dig(:conditions, :forbidden_labels) || []
  end

  def escape_issues_conditions(conditions)
    conditions.flat_map do |key, values|
      values.map { |v| "#{CGI.escape(key)}=#{CGI.escape(v)}" }
    end.join('&')
  end

  def header_string(header_label)
    if [WITHOUT_SEVERITY_STRING, WITHOUT_PRIORITY_STRING].include?(header_label)
      header_label
    else
      %(~"#{header_label}")
    end
  end

  def with_sp_labels(priority, severity)
    [
      *(priority unless priority == WITHOUT_PRIORITY_STRING),
      *(severity unless severity == WITHOUT_SEVERITY_STRING)
    ]
  end

  def without_sp_labels(priority, severity)
    [
      *(PRIORITY_LABELS if priority == WITHOUT_PRIORITY_STRING),
      *(SEVERITY_LABELS if severity == WITHOUT_SEVERITY_STRING)
    ]
  end

  def source_url_for_resource(resource, project_id: nil)
    context = Gitlab::Triage::Resource::Context.build(
      resource, network: @network)

    resource_type = context.class.name.demodulize.underscore.pluralize

    source_id = project_id || @options.source_id
    source =
      if project_id || @options.source == 'projects'
        context.__send__(:request_project, source_id)
      else
        context.__send__(:request_group, source_id)
      end

    "#{source[:web_url]}/-/#{resource_type}"
  end
end

module SummaryBuilderWithHeatMap
  private

  def description_resource
    super.merge(heat_map: LazyHeatMap.new(@resources, @policy_spec, @network))
  end
end
