# frozen_string_literal: true

require_relative 'www_gitlab_com'
require_relative 'group_definition'

module TeamMemberSelectHelper
  extend self

  IDE_GROUP_REGEXP = /ide/
  COMPOSITION_ANALYSIS_GROUP_REGEXP = /composition analysis/
  THREAT_INSIGHTS_GROUP_REGEXP = /threat insights/
  SECURITY_POLICIES_GROUP_REGEXP = /security policies/
  COMPLIANCE_GROUP_REGEXP = /compliance/
  DYNAMIC_ANALYSIS_GROUP_REGEXP = /dynamic analysis/
  BACKEND_ENGINEER_REGEXP = /backend engineer\W/
  FRONTEND_ENGINEER_REGEXP = /frontend engineer\W/
  ANY_ENGINEER_REGEXP = /(backend|frontend|fullstack) engineer\W/

  def merge_request_coaches(group: nil)
    select_team_members_by_department_specialty_role('merge request coach', nil, group, include_unavailable: false)
  end

  def select_random_merge_request_coach(group: nil)
    merge_request_coaches(group: group).sample ||
      # Pick again from the whole pool because we really want someone
      merge_request_coaches.sample
  end

  def contributor_success_team_members
    select_team_members_by_department_specialty_role(/contributor success/, nil, /contributor success/, include_ooo: false, include_unavailable: false)
  end

  def create_ide_be
    @create_ide_be ||= select_random_team_member(IDE_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP)
  end

  def create_ide_fe
    @create_ide_fe ||= select_random_team_member(IDE_GROUP_REGEXP, nil, FRONTEND_ENGINEER_REGEXP)
  end

  def composition_analysis_be(except: nil)
    @composition_analysis_be ||= select_random_team_member(COMPOSITION_ANALYSIS_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP, include_ooo: false, except: except)
  end

  def threat_insights_be(except: nil)
    @threat_insights_be ||= select_random_team_member(THREAT_INSIGHTS_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP, include_ooo: false, except: except)
  end

  def threat_insights_fe(except: nil)
    @threat_insights_fe ||= select_random_team_member(THREAT_INSIGHTS_GROUP_REGEXP, nil, FRONTEND_ENGINEER_REGEXP, include_ooo: false, except: except)
  end

  def security_policies_be(except: nil)
    @security_policies_be ||= select_random_team_member(SECURITY_POLICIES_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP, include_ooo: false, except: except)
  end

  def security_policies_fe
    @security_policies_fe ||= select_random_team_member(SECURITY_POLICIES_GROUP_REGEXP, nil, FRONTEND_ENGINEER_REGEXP)
  end

  def compliance_fs(except: nil)
    @compliance_fs ||= select_random_team_member(COMPLIANCE_GROUP_REGEXP, nil, ANY_ENGINEER_REGEXP, include_ooo: false, except: except)
  end

  def dast_be
    @dast_be ||= select_random_team_member(DYNAMIC_ANALYSIS_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP)
  end

  def select_team_members_by_department_specialty_role(department, specialty = nil, role = nil, include_unavailable: true, include_ooo: true, except: nil)
    select_team_member_usernames(include_unavailable: include_unavailable, include_ooo: include_ooo, except: except) do |data|
      user_matches?(data, department, specialty, role)
    end
  end

  def team_member_exist?(username, include_core_team_members: false)
    WwwGitLabCom.team_from_www.any? do |team_member_username, data|
      team_member_username&.downcase == username&.downcase &&
        (include_core_team_members || data['role'] != 'Core Team member')
    end
  end

  private

  def select_random_team_member(department, specialty = nil, role = nil, include_ooo: true, except: nil)
    picked = select_team_members_by_department_specialty_role(department, specialty, role, include_ooo: include_ooo, except: except).sample

    picked ||
      # Pick again from the whole pool because we really want someone
      select_team_members_by_department_specialty_role(department, specialty, role, include_ooo: true, except: except).sample
  end

  def user_matches?(data, department, specialty, role)
    department, department_matcher = field_and_matcher(department)
    specialty, specialty_matcher = field_and_matcher(specialty)
    role, role_matcher = field_and_matcher(role)

    [].tap do |results|
      results << Array(data['departments']).any?(&department_matcher) if department
      results << Array(data['specialty']).any?(&specialty_matcher) if specialty
      results << role_matcher.call(data['role']&.downcase) if role
    end.all?
  end

  def field_and_matcher(raw_field)
    if raw_field.is_a?(Regexp)
      field = Regexp.new(raw_field.source, Regexp::IGNORECASE)
      [
        field,
        ->(value) { value =~ field }
      ]
    else
      field = raw_field&.downcase
      [
        field,
        ->(value) { value.downcase == field }
      ]
    end
  end

  def select_team_member_usernames(include_unavailable: true, include_ooo: true, except: nil)
    WwwGitLabCom.team_from_www.each_with_object([]) do |(username, data), memo|
      next if !include_unavailable && unavailable?(username)

      next if !include_ooo && out_of_office?(username)

      next if username == except

      memo << "@#{username}" if yield(data)
    end
  end

  def unavailable?(username)
    unavailable_team_members.include?(username)
  end

  def unavailable_team_members
    @unavailable_team_members ||= WwwGitLabCom.roulette
      .filter_map { |data| data['username'] unless data.fetch('available', true) }
  end

  def out_of_office?(username)
    out_of_office_team_members.include?(username)
  end

  def out_of_office_team_members
    @out_of_office_team_members ||= WwwGitLabCom.roulette
      .filter_map { |data| data['username'] if data.fetch('out_of_office', false) }
  end
end
