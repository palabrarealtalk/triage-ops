# frozen_string_literal: true

require_relative 'constants/labels'
require_relative 'constants/gitlab_org'
require_relative '../triage/triage'
require_relative 'shared/resource_notes_helper'

module GrowthRefineAutomationHelper
  include ResourceNotesHelper

  GROWTH_LABELS = Labels::GROWTH_TEAM_LABELS.join(',')
  REFINE_LABEL = Labels::WORKFLOW_REFINEMENT_LABEL
  PLAN_LABEL = Labels::WORKFLOW_PLANNING_BREAKDOWN_LABEL
  GROWTH_KANBAN_BOARD_ID = 4152639
  GROWTH_REFINE_COMMENT_HEADING = '## :construction: Refinement'
  VALID_REACTIONS = { 'one' => 1, 'two' => 2, 'three' => 3, 'five' => 5, 'rocket' => 0 }.freeze
  BLOCKING_REACTION = 'x'
  MIN_REACTIONS = 3

  def growth_refinement_completed?
    return false if growth_refinement_thread.nil?

    reactions = Triage.api_client.note_award_emojis(project_id, resource_iid, 'issue', growth_refinement_thread.id)

    unique_reactions_count = count_unique_reactions(reactions)

    completed = unique_reactions_count >= MIN_REACTIONS

    if completed && !Triage.dry_run?
      Triage.api_client.create_note_award_emoji(project_id, resource_iid, 'issue', growth_refinement_thread.id, 'white_check_mark')
      max_weight = max_weight_of_reactions(reactions)
      Triage.api_client.edit_issue(project_id, resource_iid, weight: max_weight)
    end

    completed
  end

  def qualify_for_growth_refine?
    qualified_for_growth_refinement_issues_id.include?(resource_iid)
  end

  private

  def max_weight_of_reactions(reactions)
    weights = reactions.map { |reaction| VALID_REACTIONS.fetch(reaction.name, 0) }
    weights.max
  end

  def count_unique_reactions(reactions)
    return 0 if reactions.any? { |reaction| reaction.name == BLOCKING_REACTION }

    unique_user_ids = []
    unique_reactions_count = 0
    reactions&.each do |reaction|
      unless unique_user_ids.include?(reaction.user.id)
        unique_user_ids << reaction.user.id
        unique_reactions_count += 1
      end
    end

    unique_reactions_count
  end

  def find_growth_refinement_thread
    Triage.api_client.issue_notes(project_id, resource_iid).auto_paginate do |note|
      break note if note.body.start_with?(GROWTH_REFINE_COMMENT_HEADING)
    end
  end

  def growth_refinement_thread
    @growth_refinement_thread ||= find_growth_refinement_thread
  end

  def growth_refinement_max_limit
    kanban_board_lists = Triage.api_client.group_board(GitlabOrg::GROUP_ID, GROWTH_KANBAN_BOARD_ID).lists
    refinement_list = kanban_board_lists.find { |list| list.label.name == REFINE_LABEL }
    refinement_list.max_issue_count
  end

  def current_growth_refine_issue_count
    issues = Triage.api_client.group_issues(GitlabOrg::GROUP_ID, labels: "#{REFINE_LABEL},#{GROWTH_LABELS}", state: 'opened')
    issues.length
  end

  def find_qualified_growth_issues
    slots_available = growth_refinement_max_limit - current_growth_refine_issue_count
    return [] if slots_available <= 0

    planning_breakdown_issues = Triage.api_client.group_issues(GitlabOrg::GROUP_ID, labels: "#{PLAN_LABEL},#{GROWTH_LABELS}", state: 'opened', order_by: 'relative_position', sort: 'asc', per_page: slots_available)
    planning_breakdown_issues.map(&:iid)
  end

  def qualified_for_growth_refinement_issues_id
    # rubocop:disable Style/ClassVars
    @@qualified_for_growth_refinement_issues_id ||= find_qualified_growth_issues
    # rubocop:enable Style/ClassVars
  end
end
