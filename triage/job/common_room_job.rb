# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/common_room_api_client'
require_relative '../triage/job'

module Triage
  class CommonRoomJob < Job
    workers 1

    TWELVE_HOURS = 12 * 60 * 60
    DEFAULT_TIMESTAMP = '1970-01-01'

    def execute(event)
      @event = event
      @client = CommonRoomApiClient.new

      process_noteable
      process_note
    end

    def process_user(user_id)
      user_key = "user_#{user_id}"
      return if Triage.cache.set?(user_key)

      user = Triage.api_client.user(user_id)
      body = {
        user: {
          id: user_id.to_s,
          fullName: user.name,
          avatarUrl: user.avatar_url,
          bio: user.bio.empty? ? nil : user.bio,
          externalProfiles: [{ url: user.web_url }],
          email: user.public_email.nil? || user.public_email.empty? ? nil : user.public_email,
          linkedin: user.linkedin.empty? ? nil : { type: 'handle', value: user.linkedin },
          twitter: user.twitter.empty? ? nil : { type: 'handle', value: user.twitter },
          discord: nil, # Need to call the Discord API to get this!!!
          companyName: user.organization.empty? ? nil : user.organization,
          titleAtCompany: user.job_title.empty? ? nil : user.job_title,
          rawLocation: user.location.empty? ? nil : user.location
        },
        id: user_id.to_s,
        activityType: 'joined',
        timestamp: user.created_at || DEFAULT_TIMESTAMP
      }

      @client.add_activity(body)

      Triage.cache.set(user_key, true, expires_in: TWELVE_HOURS)
    end

    def process_noteable
      noteable_key = "#{event.noteable_type}_#{event.noteable_id}"
      return if Triage.cache.set?(noteable_key)

      process_user(event.resource_author_id)
      @client.add_activity(noteable)

      Triage.cache.set(noteable_key, true, expires_in: TWELVE_HOURS)
    end

    def process_note
      process_user(event.event_actor_id)
      @client.add_activity(note)
    end

    def note
      description = truncate_description(event.description)
      parent_activity_type = noteable_activity_type
      title = description.split("\n").first[0..255]
      {
        id: event.id.to_s,
        activityType: 'commented_replied',
        user: { id: event.event_actor_id.to_s },
        activityTitle: { type: 'text', value: title },
        content: { type: 'markdown', value: description },
        timestamp: event.created_at,
        url: event.url,
        tags: tags,
        subSource: { type: 'byName', name: event.payload.dig('project', 'path_with_namespace') },
        parentActivity: { activityType: parent_activity_type, id: event.noteable_id.to_s }
      }
    end

    def noteable
      {
        id: event.noteable_id.to_s,
        activityType: noteable_activity_type,
        user: { id: event.resource_author_id.to_s },
        activityTitle: { type: 'text', value: event.title },
        content: { type: 'markdown', value: event.noteable_description },
        timestamp: event.created_at,
        url: event.url,
        tags: tags,
        subSource: { type: 'byName', name: event.payload.dig('project', 'path_with_namespace') }
      }
    end

    def noteable_activity_type
      note_on_issue? ? 'created_ticket' : 'made_merge_request'
    end

    def tags
      tags = []
      tags << { type: 'name', name: event.group_label } if event.group_label
      tags << { type: 'name', name: event.type_label } if event.type_label
      tags
    end

    def truncate_description(description)
      description.to_s[0..89_999]
    end

    def note_on_issue?
      event.note_on_issue?
    end
  end
end
