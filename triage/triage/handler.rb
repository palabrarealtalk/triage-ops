# frozen_string_literal: true

require_relative 'listener'

require_relative '../processor/apply_type_label_from_related_issue'
require_relative '../processor/assign_dev_for_verification'
require_relative '../processor/availability_priority'
require_relative '../processor/backstage_label'
require_relative '../processor/breaking_change_comment'
require_relative '../processor/broken_master_label_nudger'
require_relative '../processor/copy_security_issue_labels'
require_relative '../processor/customer_label'
require_relative '../processor/default_label_upon_closing'
require_relative '../processor/deprecated_label'
require_relative '../processor/docs_only_labeller'
require_relative '../processor/engineering_allocation_labels_reminder'
require_relative '../processor/workflow/devops_labels_nudger'
require_relative '../processor/label_inference'
require_relative '../processor/label_jihu_contribution'
require_relative '../processor/legal_disclaimer_on_direction_resources'
require_relative '../processor/merge_request_ci_title_label'
require_relative '../processor/new_pipeline_on_approval'
require_relative '../processor/pajamas_missing_workflow_label_or_weight'
require_relative '../processor/prod_ops_flow_notifier'
require_relative '../processor/ceo_chief_of_staff_team_ops_notifier'
require_relative '../processor/react_to_changes'
require_relative '../processor/request_legal_review'
require_relative '../processor/require_type_on_refinement'
require_relative '../processor/revert_mr_template_nudger'
require_relative '../processor/scheduled_issue_type_label_nudger'
require_relative '../processor/seeking_community_contributions_label'
require_relative '../processor/stable_e2e_comment_on_approval'
require_relative '../processor/team_label_inference'
require_relative '../processor/type_label_nudger'
require_relative '../processor/ux_paper_cuts_mrs'

require_relative '../processor/appsec/approved_by_appsec'
require_relative '../processor/appsec/appsec_approval_label_added'
require_relative '../processor/appsec/ping_appsec_on_approval'
require_relative '../processor/appsec/revoke_appsec_approval'
require_relative '../processor/appsec/comment_on_public_mr_referencing_vulnerability'

require_relative '../processor/community/automated_review_request_doc'
require_relative '../processor/community/automated_review_request_generic'
require_relative '../processor/community/automated_review_request_ux'
require_relative '../processor/community/code_review_experience_feedback'
require_relative '../processor/community/command_mr_feedback'
require_relative '../processor/community/command_mr_handoff'
require_relative '../processor/community/command_mr_help'
require_relative '../processor/community/command_mr_label'
require_relative '../processor/community/command_mr_request_review'
require_relative '../processor/community/command_mr_unassign_review'
require_relative '../processor/community/detect_and_flag_spam'
require_relative '../processor/community/first_contribution_merged_message'
require_relative '../processor/community/growth_affecting_notifier'
require_relative '../processor/community/hackathon_label'
require_relative '../processor/community/label_leading_organization'
require_relative '../processor/community/remove_idle_labels_on_activity'
require_relative '../processor/community/reset_review_state'
require_relative '../processor/community/thank_contribution'
require_relative '../processor/community/common_room'

# GitLab Internal commands
require_relative '../processor/gitlab_internal_commands/command_retry_pipeline_or_job'

# Database
require_relative '../processor/database/database_review_experience_feedback'

# Delivery team
require_relative '../processor/delivery/security_sync_approve'
require_relative '../processor/delivery/verify_bot_approvals'

# Engineering Productivity
require_relative '../processor/engineering_productivity/gdk_issue_label'
require_relative '../processor/engineering_productivity/monitoring_event_processor'
require_relative '../processor/engineering_productivity/pipeline_failure_management'
require_relative '../processor/engineering_productivity/remind_merged_mr_deviating_from_guidelines'

# Govern:Threat Insights processors
require_relative '../processor/threat_insights/add_threat_insights_group_label'
require_relative '../processor/threat_insights/remove_threat_insights_team_labels'
require_relative '../processor/threat_insights/tag_author_if_ready_for_dev_without_weight'

# Govern:Compliance processors
require_relative '../processor/compliance_group/assign_dev_for_verification'

require_relative '../processor/support/support_team_contributions'

require_relative '../processor/workflow/closed_issue_workflow_label_updater'
require_relative '../processor/workflow/complete_when_mr_closes_issue'
require_relative '../processor/workflow/expedite_labels'
require_relative '../processor/workflow/infradev_issue_label_nudger'
require_relative '../processor/workflow/notify_merged_master_broken_merge_request'

# Growth team processors
require_relative '../processor/growth_team/add_thread_on_refinement'
require_relative '../processor/growth_team/set_milestone_on_dev'

# Docs team processors
require_relative '../processor/docs/technical_writing_label_approved'
require_relative '../processor/docs/technical_writing_label_merged'

# Privacy team processor
require_relative '../processor/privacy/denied_data_subject_request'

module Triage
  class Handler
    DEFAULT_PROCESSORS = [
      CeoChiefOfStaffTeamOpsNotifier,
      ApplyTypeLabelFromRelatedIssue,
      AssignDevForVerification,
      AvailabilityPriority,
      BackstageLabel,
      BreakingChangeComment,
      BrokenMasterLabelNudger,
      CopySecurityIssueLabels,
      CustomerLabel,
      DefaultLabelUponClosing,
      DeprecatedLabel,
      DocsOnlyLabeller,
      EngineeringAllocationLabelsReminder,
      LabelInference,
      LabelJiHuContribution,
      LegalDisclaimerOnDirectionResources,
      MergeRequestCiTitleLabel,
      NewPipelineOnApproval,
      PajamasMissingWorkflowLabelOrWeight,
      ProdOpsFlowNotifier,
      ReactToChanges,
      RemindMergedMrDeviatingFromGuideline,
      RequireTypeOnRefinement,
      RevertMrTemplateNudger,
      ScheduledIssueTypeLabelNudger,
      SeekingCommunityContributionsLabel,
      StableE2eCommentOnApproval,
      TeamLabelInference,
      TypeLabelNudger,
      UxPaperCutsMrs,

      # Internal gitlab-bot command
      CommandRetryPipelineOrJob,

      # AppSec processors
      ApprovedByAppSec,
      AppSecApprovalLabelAdded,
      PingAppSecOnApproval,
      RevokeAppSecApproval,
      CommentOnPublicMRReferencingVulnerability,

      # Community processors
      AutomatedReviewRequestDoc,
      AutomatedReviewRequestGeneric,
      AutomatedReviewRequestUx,
      CodeReviewExperienceFeedback,
      CommandMrFeedback,
      CommandMrHandoff,
      CommandMrHelp,
      CommandMrLabel,
      CommandMrRequestReview,
      CommandMrUnassignReview,
      DetectAndFlagSpam,
      FirstContributionMergedMessage,
      GrowthAffectingNotifier,
      HackathonLabel,
      LabelLeadingOrganization,
      RemoveIdleLabelOnActivity,
      ResetReviewState,
      ThankContribution,
      CommonRoom,

      # Database processors
      DatabaseReviewExperienceFeedback,

      # Delivery processors
      SecuritySyncApprove,
      VerifyBotApprovals,

      # Engineering Productivity processors
      PipelineFailureManagement,
      MonitoringEventProcessor,
      GdkIssueLabel,

      # Govern:Threat Insights processors
      AddThreatInsightsGroupLabel,
      RemoveThreatInsightsTeamLabels,
      TagAuthorIfReadyForDevWithoutWeight,

      # Govern:Compliance processors
      AssignComplianceDevForVerification,

      # Support processors
      SupportTeamContributions,

      Workflow::ClosedIssueWorkflowLabelUpdater,
      Workflow::CompleteWhenMrClosesIssue,
      Workflow::DevopsLabelsNudger,
      Workflow::ExpediteLabels,
      Workflow::InfradevIssueLabelNudger,
      Workflow::NotifyMergedMasterBrokenMergeRequest,

      # Growth team processors
      AddThreadOnRefinement,
      SetMilestoneOnDev,

      # Docs team processors
      Docs::TechnicalWritingLabelApproved,
      Docs::TechnicalWritingLabelMerged,

      # Privacy team processors
      DeniedDataSubjectRequest
    ].freeze

    Result = Struct.new(:message, :error, :duration)

    def initialize(event, processors: DEFAULT_PROCESSORS)
      @event = event
      @processors = processors
    end

    def process
      results = Hash.new { |h, k| h[k] = Result.new }

      listeners[event.key].each do |processor|
        start_time = Triage.current_monotonic_time
        results[processor.name].message = processor.triage(event)
      rescue StandardError => e
        results[processor.name].error = e
      ensure
        results[processor.name].duration = (Triage.current_monotonic_time - start_time).round(5)
      end

      results.select { |processor, result| result.message || result.error }
    end

    private

    attr_reader :event, :processors

    def listeners
      @listeners ||= processors.each_with_object(Hash.new { |h, k| h[k] = [] }) do |processor, result|
        processor.listeners.each do |listener|
          result[listener.event] << processor
        end
      end
    end
  end
end
