# frozen_string_literal: true

require_relative '../../lib/constants/labels'

module Triage
  module Strings
    module Thanks
      READY_FOR_REVIEW = %(When you're ready for a first review, post `@gitlab-bot ready`. If you know a relevant reviewer(s) (for example, someone that was involved in a related issue), you can also assign them directly with `@gitlab-bot ready @user1 @user2`.)
      REQUEST_HELP = 'At any time, if you need help, feel free to post `@gitlab-bot help` or initiate a [mentor session](https://about.gitlab.com/community/contribute/mentor-sessions/) on [Discord](https://discord.gg/gitlab). Read more on [how to get help](https://docs.gitlab.com/ee/development/contributing/#get-help).'
      GROUP_LABEL = 'You can comment `@gitlab-bot label <label1> <label2>` to add labels to your MR. Please see the list of allowed labels in the [`label` command documentation](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#reactive-label-command).'
      FIRST_CONTRIBUTION = %(Welcome to our community! We're excited to have you here, and can't wait to review this first MR with you!)
      GREETING = 'Hey @%<author_username>s! :wave:'

      RUNNER_BODY = <<~MARKDOWN.chomp
        Some contributions require several iterations of review and we try to mentor contributors
        during this process. However, we understand that some reviews can be very time consuming.
        If you would prefer for us to continue the work you've submitted now or at any point in the
        future please let us know.

        If you're okay with being part of our review process (and we hope you are!), there are
        several initial checks we ask you to make:

        * The merge request description clearly explains:
          * The problem being solved.
          * The best way a reviewer can test your changes (is it possible to provide an example?).
        * If the pipeline failed, do you need help identifying what failed?
        * Check that Go code follows our [Go guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
        * Read our [contributing to GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/CONTRIBUTING.md#contribute-to-gitlab-runner)
        document.
      MARKDOWN

      GITLAB_FOSS_BODY = <<~MARKDOWN.chomp
        Hey @%<author_username>s! :wave:

        Thank you for your contribution. GitLab has moved to a single codebase for GitLab CE and GitLab EE.

        Please do not create merge requests here. Instead, create them at https://gitlab.com/gitlab-org/gitlab/-/merge_requests.

        /close
      MARKDOWN

      WWW_GITLAB_COM_BODY = <<~MARKDOWN.chomp
        I'll notify the website team about your merge request and they will get back to you as soon
        as they can.
        If you don't hear from someone in a reasonable amount of time, please ping us again in a
        comment and mention @gitlab-com-community.
      MARKDOWN

      INTRO_THANKS_FOR_COMMUNITY_FORK = <<~MARKDOWN.chomp
        Thank you for your contribution to GitLab. Please refer to the [contribution documentation](https://docs.gitlab.com/ee/development/contributing) for an overview of the process.
      MARKDOWN

      SIGNOFF_THANKS = <<~MARKDOWN.chomp
        /label ~"#{Labels::COMMUNITY_CONTRIBUTION_LABEL}" ~"#{Labels::WORKFLOW_IN_DEV_LABEL}"
        /assign @%<author_username>s
      MARKDOWN

      COMMUNITY_FORKS = <<~MARKDOWN.chomp
        Did you know about our [community forks](https://gitlab.com/gitlab-community/meta)?
        Working from there will make your contribution process easier. Please check it out!
      MARKDOWN

      INTRO_THANKS_DEFAULT = <<~MARKDOWN.chomp
        #{INTRO_THANKS_FOR_COMMUNITY_FORK}

        #{COMMUNITY_FORKS}
      MARKDOWN

      FIRST_GREETING = <<~MARKDOWN.chomp
        #{GREETING}

        #{FIRST_CONTRIBUTION}
      MARKDOWN

      FIRST_SIGNOFF_THANKS = <<~MARKDOWN.chomp
        #{SIGNOFF_THANKS}
        /label ~"#{Labels::FIRST_CONTRIBUTION_LABEL}"
      MARKDOWN
    end
  end
end
