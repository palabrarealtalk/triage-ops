# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/processor'
require_relative '../job/scheduled_issue_type_label_nudger_job'

module Triage
  class ScheduledIssueTypeLabelNudger < Processor
    react_to 'issue.open', 'issue.update'

    FIVE_MINUTES = 300

    def applicable?
      event.added_milestone_id &&
        validator.type_label_nudge_needed?
    end

    def process
      ScheduledIssueTypeLabelNudgerJob.perform_in(FIVE_MINUTES, event)
    end

    def documentation
      <<~TEXT
        This processor posts a type label reminder to issues which are in progress or scheduled to start in 90 days.
      TEXT
    end

    private

    def validator
      @validator ||= ScheduledIssueValidator.new(event)
    end
  end
end
