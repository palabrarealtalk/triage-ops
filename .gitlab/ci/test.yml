# Yamllint of *.yml for policy files.
# This uses rules from project root `.yamllint`.
lint-policies:
  stage: test
  image: pipelinecomponents/yamllint:latest
  script:
    - yamllint -f colored .

# Rubocop of *.rb files in the project
# This uses rules from project root `.rubocop.yml`.
lint-ruby:
  extends: .ruby-before_script
  stage: test
  script:
    - bundle exec rubocop -P -E .

.test-base:
  stage: test
  extends: .ruby-before_script
  rules:
    # We don't run specs for scheduled pipelines unless it has $SCHEDULED_TESTS set.
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_TESTS == null'
      when: never
    - if: '$CI_MERGE_REQUEST_LABELS =~ /one-off/'
      when: never
    - when: on_success

regeneration-check:
  extends:
    - .test-base
  script:
    - .gitlab/ci/scripts/regeneration-check

specs:
  extends:
    - .test-base
  variables:
    BUNDLE_WITH: "coverage"
  script:
    - bundle exec rspec -Ispec -rspec_helper --color --format documentation --format RspecJunitFormatter --out ${JUNIT_RESULT_FILE}
  coverage: '/LOC \((\d+\.\d+%)\) covered.$/'
  artifacts:
    expire_in: 7d
    when: always
    paths:
      - coverage/
      - rspec/
    reports:
      junit: ${JUNIT_RESULT_FILE}
      coverage_report:
        coverage_format: cobertura
        path: ${COVERAGE_FILE}

undercoverage:
  extends:
    - .test-base
  rules:
    - if: '$CI_MERGE_REQUEST_LABELS =~ /pipeline:skip-undercoverage/'
      when: never
    - if: '$CI_MERGE_REQUEST_LABELS =~ /one-off/'
      when: never
    - if: '$CI_MERGE_REQUEST_IID'
  variables:
    BUNDLE_WITH: "coverage"
  needs: ["specs"]
  script:
    - git fetch ${CI_MERGE_REQUEST_SOURCE_PROJECT_URL} ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME};
    - if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        echo "Checking out \$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA ($CI_MERGE_REQUEST_SOURCE_BRANCH_SHA) instead of \$CI_COMMIT_SHA (merge result commit $CI_COMMIT_SHA) so we can use $CI_MERGE_REQUEST_DIFF_BASE_SHA for undercoverage in this merged result pipeline";
        git checkout -f ${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA};
        bundle_install_script;
      else
        echo "Using \$CI_COMMIT_SHA ($CI_COMMIT_SHA) for this non-merge result pipeline.";
      fi;
    - UNDERCOVERAGE_COMPARE="${CI_MERGE_REQUEST_DIFF_BASE_SHA:-$(git merge-base origin/${CI_DEFAULT_BRANCH} HEAD)}"
    - echo "Undercoverage comparing with ${UNDERCOVERAGE_COMPARE}"
    - bundle exec undercover -c "${UNDERCOVERAGE_COMPARE:-$(git merge-base origin/${CI_DEFAULT_BRANCH} HEAD)}"
  artifacts:
    expire_in: 7d
    when: always
    paths:
      - coverage/
