# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/dev_master_branch'

RSpec.describe Triage::PipelineFailure::Config::DevMasterBranch do
  let(:project_path_with_namespace) { 'gitlab/gitlab-ee' }
  let(:ref) { 'master' }
  let(:source_job_id) { nil }
  let(:mr_pipeline?) { false }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      project_path_with_namespace: project_path_with_namespace,
      ref: ref,
      merge_request_pipeline?: mr_pipeline?,
      source_job_id: source_job_id)
  end

  before do
    allow(event).to receive(:on_instance?).with(:dev).and_return(true)
  end

  subject { described_class.new(event) }

  describe '.match?' do
    it 'returns true' do
      expect(described_class.match?(event)).to be_truthy
    end

    context 'when event is not on the dev.gitlab.org instance' do
      before do
        allow(event).to receive(:on_instance?).with(:dev).and_return(false)
      end

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when project_path_with_namespace is not "gitlab-org/gitlab"' do
      let(:project_path_with_namespace) { 'foo/bar' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when ref is not "master"' do
      let(:ref) { 'foo' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when source_job_id is present' do
      let(:source_job_id) { '42' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when triggered by a merge request pipeline' do
      let(:mr_pipeline?) { true }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end
  end

  describe '#incident_project_id' do
    it 'returns expected projet id' do
      expect(subject.incident_project_id).to eq('5064907')
    end
  end

  describe '#incident_template' do
    it 'returns expected template' do
      expect(subject.incident_template).to eq(described_class::INCIDENT_TEMPLATE)
    end
  end

  describe '#incident_labels' do
    it 'returns expected labels' do
      expect(subject.incident_labels).to eq(%w[release-blocker dev-failure])
    end
  end

  describe '#default_slack_channels' do
    it 'returns expected channel' do
      expect(subject.default_slack_channels).to eq(['master-broken-mirrors'])
    end
  end

  describe '#auto_triage?' do
    it 'returns true' do
      expect(subject.auto_triage?).to be_truthy
    end
  end
end
