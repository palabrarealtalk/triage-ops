# frozen_string_literal: true

require 'spec_helper'
require 'tempfile'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/triage_incident'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'

GITLAB_PROJECT_ID = Triage::Event::GITLAB_PROJECT_ID
FIXTURE_PATH      = '/reactive/job_traces'

RSpec.describe Triage::PipelineFailure::TriageIncident do
  include_context 'with data files stubbed'

  let(:event)      { instance_double(Triage::PipelineEvent, id: 123, project_id: GITLAB_PROJECT_ID, instance: :com) }
  let(:job1)       { double(id: '1', name: 'job_a', web_url: 'a_web_url', allow_failure: false) }
  let(:job2)       { double(id: '2', name: 'job_b', web_url: 'b_web_url', allow_failure: false) }
  let(:job3)       { double(id: '3', name: 'job_c', web_url: 'c_web_url', allow_failure: false) }
  let(:job4)       { double(id: '4', name: 'job_d', web_url: 'd_web_url', allow_failure: false) }
  let(:job5)       { double(id: '5', name: 'job_e', web_url: 'e_web_url', allow_failure: false) }
  let(:job6)       { double(id: '6', name: 'job_f', web_url: 'f_web_url', allow_failure: false) }

  let(:workhorse_job) { double(id: '7', name: 'workhorse:test go', web_url: 'workhorse_job_web_url', allow_failure: false) }

  let(:api_client_double)           { double('gitlab_api_client') }
  let(:failed_to_pull_image_trace)  { read_fixture("#{FIXTURE_PATH}/failed_to_pull_image.txt") }
  let(:infra_trace)                 { read_fixture("#{FIXTURE_PATH}/infrastructure_error.txt") }
  let(:gitlab_com_overloaded_trace) { read_fixture("#{FIXTURE_PATH}/gitlab_com_overloaded.txt") }
  let(:runner_disk_full_trace)      { read_fixture("#{FIXTURE_PATH}/runner_disk_full.txt") }
  let(:pg_query_canceled_trace)     { read_fixture("#{FIXTURE_PATH}/pg_query_canceled.txt") }
  let(:workhorse_job_trace)         { read_fixture("#{FIXTURE_PATH}/workhorse-test.txt") }
  let(:failed_jobs)                 { [] }
  let(:config)                      { Triage::PipelineFailure::Config::MasterBranch.new(event) }

  let(:job_artifact_file) { Gitlab::FileResponse.new(Tempfile.new("/reactive/job_traces/rspec.json")) }

  subject { described_class.new(event: event, failed_jobs: failed_jobs, config: config) }

  before do
    allow(Triage).to receive(:api_client).and_return(api_client_double)
    allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, job1.id).and_return(failed_to_pull_image_trace)
    allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, job2.id).and_return(infra_trace)
    allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, job3.id).and_return(gitlab_com_overloaded_trace)
    allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, job4.id).and_return('')
    allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, job5.id).and_return(runner_disk_full_trace)
    allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, job6.id).and_return(pg_query_canceled_trace)
    allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, workhorse_job.id).and_return(workhorse_job_trace)

    allow(api_client_double).to receive(:download_job_artifact_file).and_return(nil)
  end

  def duplicate_jobs(template_job, count, trace)
    (1..count).map do |id|
      allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, id).and_return(trace)
      double(id: id, name: template_job.name, web_url: template_job.web_url, instance: :dev)
    end
  end

  describe '#top_root_cause_label' do
    context 'with no failed jobs' do
      it 'returns master-broken::undetermined' do
        expect(subject.top_root_cause_label).to eq('master-broken::undetermined')
      end
    end

    context 'with only 1 job failure which is caused by unknown reason' do
      let(:failed_jobs) { [job4] }

      it 'returns master-broken::undetermined' do
        expect(subject.top_root_cause_label).to eq('master-broken::undetermined')
      end
    end

    context 'with only 1 job failure which is caused by runner-disk-full' do
      let(:failed_jobs) { [job5] }

      it 'returns master-broken::runner-disk-full' do
        expect(subject.top_root_cause_label).to eq('master-broken::runner-disk-full')
      end
    end

    context 'with mulitple jobs failed due to failed-to-pull-image' do
      let(:failed_jobs) { duplicate_jobs(job1, 10, failed_to_pull_image_trace) }

      it 'returns master-broken::failed-to-pull-image' do
        expect(subject.top_root_cause_label).to eq('master-broken::failed-to-pull-image')
      end
    end

    # rubocop:disable RSpec/MultipleMemoizedHelpers
    describe '#top_group_label' do
      let(:job6) { double(id: '6', name: 'job_f', web_url: 'e_web_url', allow_failure: false) }
      let(:job7) { double(id: '7', name: 'job_g', web_url: 'e_web_url', allow_failure: false) }

      let(:rspec_full_trace) { read_fixture("#{FIXTURE_PATH}/rspec_failure.txt") }
      let(:job_artifact_file_2) { Gitlab::FileResponse.new(Tempfile.new("/reactive/job_traces/rspec-2.json")) }

      before do
        allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, job6.id).and_return(rspec_full_trace)
        allow(api_client_double).to receive(:job_trace).with(GITLAB_PROJECT_ID, job7.id).and_return(rspec_full_trace)
      end

      context 'with no failed jobs' do
        it 'returns master-broken::undetermined' do
          expect(subject.top_group_label).to be_nil
        end
      end

      context 'with only 1 job failure which is caused by runner-disk-full' do
        let(:failed_jobs) { [job5] }

        it 'returns no top_group_label' do
          expect(subject.top_group_label).to be_nil
        end
      end

      context 'with only 1 RSpec job failure' do
        let(:failed_jobs) { [job6] }

        before do
          job_artifact_file.write(read_fixture('/reactive/job_traces/rspec.json'))
          job_artifact_file.rewind

          allow(api_client_double).to receive(:download_job_artifact_file).with(GITLAB_PROJECT_ID, "6", "rspec/rspec-6.json").and_return(job_artifact_file)
        end

        after do
          job_artifact_file.unlink
        end

        it 'returns the corresponding group label' do
          expect(subject.top_group_label).to eq('group::group1')
        end
      end

      context 'with 2 RSpec job failure' do
        let(:failed_jobs) { [job6, job7] }

        before do
          job_artifact_file.write(read_fixture('/reactive/job_traces/rspec.json'))
          job_artifact_file.rewind

          job_artifact_file_2.write(read_fixture('/reactive/job_traces/rspec-2.json'))
          job_artifact_file_2.rewind

          allow(api_client_double).to receive(:download_job_artifact_file).with(GITLAB_PROJECT_ID, "6", "rspec/rspec-6.json").and_return(job_artifact_file)
          allow(api_client_double).to receive(:download_job_artifact_file).with(GITLAB_PROJECT_ID, "7", "rspec/rspec-7.json").and_return(job_artifact_file_2)
        end

        after do
          job_artifact_file.unlink
          job_artifact_file_2.unlink
        end

        it 'returns the corresponding group label' do
          expect(subject.top_group_label).to eq('group::group1')
        end
      end
    end
    # rubocop:enable RSpec/MultipleMemoizedHelpers

    context 'with most jobs failed due to failed-to-pull-image except 1 unknown root cause' do
      let(:failed_jobs) { duplicate_jobs(job1, 9, failed_to_pull_image_trace).push(job4) }

      it 'returns master-broken::failed-to-pull-image' do
        expect(subject.top_root_cause_label).to eq('master-broken::failed-to-pull-image')
      end
    end

    context 'with some repeated transient errors and some non-repeated, non-transient errors' do
      let(:failed_jobs) { [job1, job1, job2, job3, job4, job5, job6] }

      it 'return the most repeated root cause label' do
        expect(subject.top_root_cause_label).to eq('master-broken::failed-to-pull-image')
      end
    end

    context 'when every job has a different root cause' do
      let(:failed_jobs) { [job1, job2, job3, job4, job5, job6] }

      it 'returns any root cause besides master-broken::undetermined' do
        expect(subject.top_root_cause_label).not_to eq('master-broken::undetermined')
      end
    end
  end

  describe '#root_cause_analysis_comment' do
    before do
      allow(api_client_double).to receive(:job_retry).and_return({ 'web_url' => 'retry_job_web_url' })
      allow(api_client_double).to receive(:retry_pipeline).and_return({ 'web_url' => 'retried_pipeline_web_url' })
    end

    context 'with 10 failed jobs, all due to a known transient error' do
      let(:failed_jobs) { duplicate_jobs(job1, 10, failed_to_pull_image_trace) }

      it 'determines root cause from trace, retries pipeline, and closes incident' do
        expect(Triage.api_client).to receive(:job_trace).exactly(10).times
        expect(Triage.api_client).to receive(:retry_pipeline).once
        expect(Triage.api_client).not_to receive(:job_retry)

        expect(subject.root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".

            Retried pipeline: retried_pipeline_web_url

            This incident is caused by known transient error(s), closing.
          MARKDOWN
        )
      end
    end

    context 'with 10 failed jobs, some have unknown root causes' do
      let(:failed_jobs) { duplicate_jobs(job1, 9, failed_to_pull_image_trace).push(job4) }

      it 'labels each job, does not retry pipeline or close incident' do
        expect(Triage.api_client).to receive(:job_trace).exactly(10).times
        expect(Triage.api_client).not_to receive(:retry_pipeline)
        expect(Triage.api_client).not_to receive(:job_retry)

        expect(subject.root_cause_analysis_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_a](a_web_url): ~"master-broken::failed-to-pull-image".
            - [job_d](d_web_url): ~"master-broken::undetermined".
          MARKDOWN
        )
      end
    end

    context 'with less than 10 failed jobs' do
      let(:failed_jobs) { duplicate_jobs(job1, 9, failed_to_pull_image_trace) }

      it 'fetches all traces and retries failed jobs individually' do
        expect(Triage.api_client).to receive(:job_trace).exactly(9).times
        expect(Triage.api_client).to receive(:job_retry).exactly(9).times
        expect(Triage.api_client).not_to receive(:retry_pipeline)

        expect(subject.root_cause_analysis_comment).not_to include(
          'Retried pipeline: retried_pipeline_web_url'
        )
      end

      context 'with no failed job (e.g. if the CI config is invalid)' do
        let(:failed_jobs) { [] }

        it 'returns nil' do
          expect(subject.root_cause_analysis_comment).to be_nil
        end
      end

      context 'with job trace returning nil (e.g. request failed)' do
        let(:failed_jobs) { [job2] }

        before do
          allow(api_client_double).to receive(:job_trace).and_return(nil)
        end

        it 'considers the job trace empty and labels root cause as undetermined' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job_b](b_web_url): ~"master-broken::undetermined".
            MARKDOWN
          )
        end
      end

      context 'with 1 failed job due to a known transient error' do
        let(:failed_jobs) { [job2] }

        it 'retries the job and closes incident' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job_b](b_web_url): ~"master-broken::infrastructure". Retried at: retry_job_web_url

              This incident is caused by known transient error(s), closing.
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected with some repeated causes and some non-transient errors' do
        let(:failed_jobs) { [job1, job1, job2, job3, job4, job5] }

        it 'only retries each job failed with transient error' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job_a](a_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_job_web_url
              - [job_a](a_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_job_web_url
              - [job_b](b_web_url): ~"master-broken::infrastructure". Retried at: retry_job_web_url
              - [job_c](c_web_url): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_e](e_web_url): ~"master-broken::runner-disk-full". Retried at: retry_job_web_url
            MARKDOWN
          )
        end
      end

      context 'when multiple root causes detected, all caused by transient errors' do
        let(:failed_jobs) { [job1, job2, job3, job3, job5] }

        it 'labels and retries each job, and closes the incident' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job_a](a_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_job_web_url
              - [job_b](b_web_url): ~"master-broken::infrastructure". Retried at: retry_job_web_url
              - [job_c](c_web_url): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job_c](c_web_url): ~"master-broken::gitlab-com-overloaded". Retried at: retry_job_web_url
              - [job_e](e_web_url): ~"master-broken::runner-disk-full". Retried at: retry_job_web_url

              This incident is caused by known transient error(s), closing.
            MARKDOWN
          )
        end
      end

      context 'when a workhorse job failed' do
        let(:failed_jobs) { [workhorse_job] }

        it 'does not retry any job and labels the job master-broken::undermined' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [workhorse:test go](workhorse_job_web_url): ~"master-broken::undetermined".
            MARKDOWN
          )
        end
      end

      context 'when none of the job failures are recognized' do
        let(:failed_jobs) { duplicate_jobs(job4, 4, '') }

        it 'does not retry any job and labels each job master-broken::undetermined' do
          expect(subject.root_cause_analysis_comment).to eq(
            <<~MARKDOWN.chomp.prepend("\n\n")
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_d](d_web_url): ~"master-broken::undetermined".
              - [job_d](d_web_url): ~"master-broken::undetermined".
            MARKDOWN
          )
        end
      end
    end
  end

  describe "#attribution_comment" do
    context 'with rspec failures' do
      let(:failed_jobs)    { [double(id: '6', name: 'job_with_rspec_failure', web_url: 'rspec_job_web_url', allow_failure: false)] }
      let(:rspec_failure)  { read_fixture("#{FIXTURE_PATH}/rspec_failure.txt") }

      before do
        job_artifact_file.write(read_fixture('/reactive/job_traces/rspec.json'))
        job_artifact_file.rewind
      end

      after do
        job_artifact_file.unlink
      end

      it 'prints the job id and feature category' do
        allow(api_client_double).to receive(:job_trace).and_return(rspec_failure)
        allow(api_client_double).to receive(:download_job_artifact_file).with(GITLAB_PROJECT_ID, "6", "rspec/rspec-6.json").and_return(job_artifact_file)

        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/user_adds_lists_to_board_spec.rb:10
            - ~"group::group1" ~"Category:2" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]
            - ~"group::group1" ~"Category:1" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:4]
            - ~"group::group1" ~"Category:1" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:5]
            - ~"group::group2" ~"Category:3" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:6]

            **This incident is attributed to ~"group::group1" and posted in `#group1`.**
          MARKDOWN
        )
      end
    end

    context 'with worhorse job failure' do
      let(:failed_jobs) { [workhorse_job] }
      let(:workhorse_log) { read_fixture("#{FIXTURE_PATH}/workhorse-test.txt") }

      before do
        allow(WwwGitLabCom).to receive(:groups).and_return({
          'source_code' => { 'label' => 'group::source code', 'slack_channel' => 'g_create_source-code' }
        })
      end

      it 'attributes to group::source code' do
        allow(api_client_double).to receive(:job_trace).and_return(workhorse_log)

        expect(subject.attribution_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - ~"group::source code" workhorse:test go

            **This incident is attributed to ~"group::source code" and posted in `#g_create_source-code`.**
          MARKDOWN
        )
      end
    end
  end

  describe '#investigation_comment' do
    context 'with job trace returning nil' do
      let(:failed_jobs) { [job4] }

      it 'returns nil' do
        allow(api_client_double).to receive(:job_trace).and_return(nil)
        expect(subject.investigation_comment).to be_nil
      end
    end

    context 'with 1 job failed on trasient error' do
      let(:failed_jobs) { [job1] }

      it 'returns nil' do
        expect(subject.investigation_comment).to be_nil
      end
    end

    context 'with 1 failed job showing rspec failures' do
      let(:failed_jobs)    { [double(id: '6', name: 'job_with_rspec_failure', web_url: 'rspec_job_web_url', allow_failure: false)] }
      let(:rspec_failure)  { read_fixture("#{FIXTURE_PATH}/rspec_failure.txt") }

      before do
        job_artifact_file.write(read_fixture('/reactive/job_traces/rspec.json'))
        job_artifact_file.rewind
      end

      after do
        job_artifact_file.unlink
      end

      it 'prints test log' do
        allow(api_client_double).to receive(:job_trace).and_return(rspec_failure)
        allow(api_client_double).to receive(:download_job_artifact_file).with(GITLAB_PROJECT_ID, "6", "rspec/rspec-6.json").and_return(job_artifact_file)

        expect(subject.investigation_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job_with_rspec_failure](rspec_job_web_url):

            ```ruby

              1) Merge request > User merges when pipeline succeeds when there is active pipeline for merge request with auto_merge_labels_mr_widget on enabling Merge when pipeline succeeds when it was enabled and then canceled behaves like Set to auto-merge activator activates the Merge when pipeline succeeds feature
                 Got 0 failures and 2 other errors:
                 Shared Example Group: "Set to auto-merge activator" called from ./spec/features/merge_request/user_merges_when_pipeline_succeeds_spec.rb:161

                 1.1) Failure/Error: super

                      Capybara::ElementNotFound:
                        Unable to find button "Set to auto-merge" that is not disabled

                        Timeout (45s) reached while running a waiting Capybara finder.
                      # ------------------
                      # --- Caused by: ---
                      # Capybara::ElementNotFound:
                      #   Unable to find button "Set to auto-merge" that is not disabled
                      #   ./spec/support/capybara_slow_finder.rb:18:in `synchronize'

            Finished in 2 minutes 6.7 seconds (files took 1 minute 2.1 seconds to load)
            1 example, 1 failure

            Failed examples:

            rspec './spec/features/merge_request/user_merges_when_pipeline_succeeds_spec.rb[1:2:1:3:1:1]' # Merge request > User merges when pipeline succeeds when there is active pipeline for merge request with auto_merge_labels_mr_widget on enabling Merge when pipeline succeeds when it was enabled and then canceled behaves like Set to auto-merge activator activates the Merge when pipeline succeeds feature
            ```

            /label ~backend
          MARKDOWN
        )
      end
    end

    context 'with 1 failed job showing jest failures' do
      let(:failed_jobs) { [double(id: '7', name: 'job_with_jest_failure', web_url: 'jest_job_web_url', allow_failure: false)] }
      let(:jest_failure)  { read_fixture("#{FIXTURE_PATH}/jest_failure.txt") }

      it 'prints test log' do
        allow(api_client_double).to receive(:job_trace).and_return(jest_failure)

        expect(subject.investigation_comment).to eq(
          <<~MARKDOWN.chomp.prepend("\n\n")
            - [job_with_jest_failure](jest_job_web_url):

            ```javascript
            FAIL spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js
              ● vue_shared/component/markdown/markdown_editor › disabled › disables content editor when disabled prop is true

                : Timeout - Async callback was not invoked within the 5000 ms timeout specified by jest.setTimeout.Timeout - Async callback was not invoked within the 5000 ms timeout specified by jest.setTimeout.Error:

                  148 |     });
                  149 |
                > 150 |     it('disables content editor when disabled prop is true', async () => {
                      |     ^
                  151 |       buildWrapper({ propsData: { disabled: true } });
                  152 |
                  153 |       await enableContentEditor();

                  at new Spec (node_modules/jest-jasmine2/build/jasmine/Spec.js:124:22)
                  at Suite.it (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:150:5)
                  at Suite.describe (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:137:3)
                  at Object.describe (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:19:1)


            Test Suites: 1 failed, 532 passed, 533 total
            Tests:       1 failed, 2 skipped, 32 todo, 7782 passed, 7817 total
            Snapshots:   56 passed, 56 total
            Time:        1332.608 s
            ```

            /label ~frontend
          MARKDOWN
        )
      end
    end
  end

  describe '#duplicate_incident_iid' do
    let(:previous_incidents)  { [] }
    let(:incident_project_id) { '40549124' }
    let(:failed_jobs)         { [job1] }

    before do
      allow(api_client_double).to receive(:issues).with(
        incident_project_id, { order_by: 'created_at', per_page: 1, sort: "desc" }
      ).and_return(previous_incidents)
    end

    context 'when there was no previous incidents' do
      it 'returns nil' do
        expect(subject.duplicate_incident_iid).to be_nil
      end
    end

    # rubocop:disable RSpec/MultipleMemoizedHelpers
    context 'when the previous incident did not fail with the same job' do
      let(:previous_incident_title) { 'Wednesday 2023-09-27 17:47 UTC - `gitlab-org/gitlab` broken `master` with rspec-ee system pg14 4/10' }
      let(:previous_incidents) { [double('Incident', iid: 2, title: previous_incident_title)] }

      it 'returns nil' do
        expect(subject.duplicate_incident_iid).to be_nil
      end
    end

    context 'when only the previous incident failed with the same jobs' do
      let(:previous_incident_title) { 'Wednesday 2023-09-27 17:47 UTC - `gitlab-org/gitlab` broken `master` with job_a' }
      let(:previous_incident) do
        double('Incident', iid: 2, title: previous_incident_title, _links: {})
      end

      let(:previous_incidents) { [previous_incident] }

      it 'returns nil' do
        expect(subject.duplicate_incident_iid).to be_nil
      end
    end

    context 'when the previous incident failed with the same jobs and was closed as a duplicate of an earlier incident' do
      let(:previous_incident_title) { 'Wednesday 2023-09-27 17:47 UTC - `gitlab-org/gitlab` broken `master` with job_a' }
      let(:links) { { 'closed_as_duplicate_of' => "http://gitlab.example.com/api/v4/projects/1/issues/1" } }
      let(:previous_incident) do
        double('Incident', iid: 1, title: previous_incident_title, _links: links)
      end

      let(:previous_incidents) { [previous_incident] }

      it 'returns the iid of the that earlier incident' do
        expect(subject.duplicate_incident_iid).to eq(1)
      end
    end
    # rubocop:enable RSpec/MultipleMemoizedHelpers
  end
end
