# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/processor'
require_relative '../../triage/triage/rate_limit'

RSpec.describe Triage::RateLimit do
  include_context 'with event' do
    let(:event_attrs) do
      {
        object_kind: 'issue',
        action: 'open'
      }
    end
  end

  let(:mock) { double('mock', call: true) }

  let(:processor) do
    Class.new(Triage::Processor) do
      include Triage::RateLimit

      react_to 'issue.*'

      def initialize(event, mock)
        super(event)
        @mock = mock
      end

      def cache_key
        "test-key-#{event.event_actor_id}"
      end

      def process
        @mock.call
      end
    end
  end

  subject { processor.new(event, mock) }

  describe 'DEFAULT_RATE_LIMIT_COUNT' do
    it { expect(described_class::DEFAULT_RATE_LIMIT_COUNT).to eq(60) }
  end

  describe 'DEFAULT_RATE_LIMIT_PERIOD' do
    it { expect(described_class::DEFAULT_RATE_LIMIT_PERIOD).to eq(3600) }
  end

  describe 'rate limiting', :clean_cache do
    it 'records the number of events sent by author and resets the counter after 1 hour' do
      count = described_class::DEFAULT_RATE_LIMIT_COUNT
      period = described_class::DEFAULT_RATE_LIMIT_PERIOD

      start_time = Time.now

      # exhaust rate limit
      count.times { subject.triage }

      expect(mock).to have_received(:call).exactly(count).times

      # Subsequent events are rate limited
      subject.triage
      expect(mock).to have_received(:call).exactly(count).times

      # 1 second before rate limit expiry time, still rate limited
      Timecop.freeze(start_time + period - 1) do
        subject.triage
        expect(mock).to have_received(:call).exactly(count).times
      end

      # at rate limit expiry time, still rate limited
      Timecop.freeze(start_time + period) do
        subject.triage
        expect(mock).to have_received(:call).exactly(count).times
      end

      # 1 second after rate limit expiry time, rate limit released
      Timecop.freeze(start_time + period + 1) do
        subject.triage
        expect(mock).to have_received(:call).exactly(count + 1).times
      end
    end

    it 'tracks separate user id on a separate rate limit counter' do
      stub_const('Triage::RateLimit::DEFAULT_RATE_LIMIT_COUNT', 2)

      mock2 = double('mock', call: true)
      event2 = stubbed_event(event_actor_id: 2, event_class: Triage::Event)
      subject2 = processor.new(event2, mock2)

      start_time = Time.now

      subject.triage
      expect(mock).to have_received(:call).once
      expect(mock2).not_to have_received(:call)

      halfway_period = described_class::DEFAULT_RATE_LIMIT_PERIOD / 2

      Timecop.freeze(start_time + halfway_period) do
        subject.triage
        expect(mock).to have_received(:call).twice
      end

      Timecop.freeze(start_time + halfway_period + 1) do
        subject.triage
        subject2.triage

        expect(mock).to have_received(:call).twice
        expect(mock2).to have_received(:call).once
      end
    end

    it 'does not process event if user exceeds the allowed limit' do
      stub_const("#{described_class}::DEFAULT_RATE_LIMIT_COUNT", 1)

      subject.triage
      subject.triage

      expect(mock).to have_received(:call).at_most(:once)
    end
  end

  describe 'with custom rate limit', :clean_cache do
    let(:processor) do
      Class.new(Triage::Processor) do
        include Triage::RateLimit

        react_to 'issue.*'

        def initialize(event, mock)
          super(event)
          @mock = mock
        end

        def cache_key
          "test-key-#{event.event_actor['id']}"
        end

        def rate_limit_count
          1
        end

        def rate_limit_period
          60
        end

        def process
          @mock.call
        end
      end
    end

    it 'rate limits events sent by author and resets after the period has passed' do
      subject.triage
      subject.triage

      expect(mock).to have_received(:call).once

      Timecop.travel(Time.now + 61) do
        subject.triage

        expect(mock).to have_received(:call).twice
      end
    end
  end
end
