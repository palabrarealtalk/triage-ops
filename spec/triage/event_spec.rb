# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage'
require_relative '../../triage/triage/event'

RSpec.describe Triage::Event do
  subject { described_class.build(event) }

  let(:fixture_path) { 'note_on_issue.json' }
  let(:event) do
    JSON.parse(read_fixture("/reactive/#{fixture_path}"))
  end

  let(:labels) { event['labels'].map { |label| label['title'] } }
  let(:current_labels) { event['changes']['labels']['current'].map { |label| label['title'] } }
  let(:previous_labels) { event['changes']['labels']['previous'].map { |label| label['title'] } }

  describe '.build' do
    context 'when the event does not have an "object_attributes" key' do
      let(:event) { { 'object_kind' => 'issue' } }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::IssueEvent)
      end
    end

    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns a IssueEvent' do
        expect(subject).to be_a(Triage::IssueEvent)
      end
    end

    context 'when the event is a new incident' do
      let(:fixture_path) { 'new_incident.json' }

      it 'returns a IncidentEvent' do
        expect(subject).to be_a(Triage::IncidentEvent)
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns a MergeRequestEvent' do
        expect(subject).to be_a(Triage::MergeRequestEvent)
      end
    end

    context 'when the event is a note on an issue' do
      it 'returns a NoteEvent' do
        expect(subject).to be_a(Triage::NoteEvent)
      end
    end

    context 'when the event is a note on a merge request' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it 'returns a NoteEvent' do
        expect(subject).to be_a(Triage::NoteEvent)
      end
    end

    context 'when object_kind is not provided' do
      let(:event) { {} }

      it 'raises ObjectKindNotProvidedError' do
        expect { subject }.to raise_error(described_class::ObjectKindNotProvidedError)
      end
    end

    context 'when object_kind is unsupported' do
      let(:event) { { 'object_kind' => described_class::PUSH } }

      it 'raises UnsupportedObjectKind' do
        expect { subject }.to raise_error(described_class::UnsupportedObjectKind)
      end
    end

    context 'when object_kind is unknown' do
      let(:event) { { 'object_kind' => 'unknown' } }

      it 'raises UnknownObjectKind' do
        expect { subject }.to raise_error(described_class::UnknownObjectKind)
      end
    end
  end

  describe '#key' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns issue.open' do
        expect(subject.key).to eq("issue.open")
      end
    end

    context 'with monitoring event and uptime_check action' do
      let(:event) { { 'object_kind' => 'monitoring', 'object_attributes' => { 'action' => 'uptime_check' } } }

      it 'return monitoring.uptime_check' do
        expect(subject.key).to eq("monitoring.uptime_check")
      end
    end
  end

  describe '#issue?' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns true' do
        expect(subject).to be_issue
      end
    end

    context 'when the event is a new incident' do
      let(:fixture_path) { 'new_incident.json' }

      it 'returns false' do
        expect(subject).not_to be_issue
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns false' do
        expect(subject).not_to be_issue
      end
    end
  end

  describe '#incident?' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns false' do
        expect(subject).not_to be_incident
      end
    end

    context 'when the event is a new incident' do
      let(:fixture_path) { 'new_incident.json' }

      it 'returns true' do
        expect(subject).to be_incident
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns false' do
        expect(subject).not_to be_incident
      end
    end
  end

  describe '#merge_request?' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns false' do
        expect(subject).not_to be_merge_request
      end
    end

    context 'when the event is a new incident' do
      let(:fixture_path) { 'new_incident.json' }

      it 'returns false' do
        expect(subject).not_to be_merge_request
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns true' do
        expect(subject).to be_merge_request
      end
    end
  end

  describe '#note?' do
    context 'when the event is not a note' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns false' do
        expect(subject).not_to be_note
      end
    end

    context 'when the event is a new note on an issue' do
      it 'returns true' do
        expect(subject).to be_note
      end
    end

    context 'when the event is a new note on a MR' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it 'returns true' do
        expect(subject).to be_note
      end
    end
  end

  describe '#instance' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when the event is on the .com instance' do
      it 'returns :com' do
        expect(subject.instance).to eq(:com)
      end
    end

    context 'when the event is on the dev.gitlab.org instance' do
      before do
        event['project']['web_url'] = 'https://dev.gitlab.org/foo/bar'
      end

      it 'returns :dev' do
        expect(subject.instance).to eq(:dev)
      end
    end

    context 'when the event is on an unknown instance' do
      before do
        event['project']['web_url'] = 'https://foo.gitlab.bar/foo/bar'
      end

      it 'raises error' do
        expect { subject.instance }.to raise_error("Unknown instance for 'https://foo.gitlab.bar/foo/bar'")
      end
    end
  end

  describe '#on_instance?' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when the event is on the .com instance' do
      describe 'we check for #on_instance?(:com)' do
        it 'returns true' do
          expect(subject.on_instance?(:com)).to be(true)
        end
      end

      describe 'we check for #on_instance?(:dev)' do
        it 'returns false' do
          expect(subject.on_instance?(:dev)).to be(false)
        end
      end
    end

    context 'when the event is on the dev.gitlab.org instance' do
      before do
        event['project']['web_url'] = 'https://dev.gitlab.org/foo/bar'
      end

      describe 'we check for #on_instance?(:com)' do
        it 'returns false' do
          expect(subject.on_instance?(:com)).to be(false)
        end
      end

      describe 'we check for #on_instance?(:dev)' do
        it 'returns true' do
          expect(subject.on_instance?(:dev)).to be(true)
        end
      end
    end

    describe 'when we check for #on_instance?(:unknown)' do
      it 'returns false' do
        expect(subject.on_instance?(:unknown)).to be(false)
      end
    end
  end

  describe '#event_actor' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns a User object' do
        expect(subject.event_actor).to be_a(Triage::User)
        expect(subject.event_actor.to_h).to eq(event['user'])
        expect(subject.event_actor['username']).to eq(event['user']['username'])
      end
    end
  end

  describe '#event_actor_id' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns object_attributes.user.id' do
        expect(subject.event_actor_id).to eq(event['user']['id'])
      end
    end
  end

  describe '#event_actor_username' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns object_attributes.user.username' do
        expect(subject.event_actor_username).to eq(event['user']['username'])
      end
    end
  end

  describe '#resource_author_id' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns object_attributes.author_id' do
        expect(subject.resource_author_id).to eq(event['object_attributes']['author_id'])
      end
    end
  end

  describe '#resource_author' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }
      let(:user_attrs) { { 'id' => event['object_attributes']['author_id'], 'username' => 'root' } }

      it 'returns a User object that corresponds to the object_attributes.author_id' do
        expect_api_request(path: "/users/#{user_attrs['id']}", response_body: user_attrs) do
          expect(subject.resource_author).to be_a(Triage::User)
          expect(subject.resource_author.to_h).to eq(user_attrs)
          expect(subject.resource_author.id).to eq(user_attrs['id'])
          expect(subject.resource_author.username).to eq(user_attrs['username'])
        end
      end
    end
  end

  describe '#by_resource_author?' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when event is from the resource author' do
      it 'returns true' do
        event['object_attributes']['author_id'] = event['user']['id']

        expect(subject.by_resource_author?).to be(true)
      end
    end

    context 'when event is not from the resource author' do
      it 'returns false' do
        event['object_attributes']['author_id'] = event['user']['id'] + 1

        expect(subject.by_resource_author?).to be(false)
      end
    end
  end

  describe '#by_resource_assignee?' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when event is from the resource assignee' do
      it 'returns true' do
        event['object_attributes']['assignee_ids'] = [event['user']['id']]

        expect(subject.by_resource_assignee?).to be(true)
      end
    end

    context 'when event is not from the resource assignee' do
      it 'returns true' do
        event['object_attributes']['assignee_ids'] = [event['user']['id'] + 1]

        expect(subject.by_resource_assignee?).to be(false)
      end
    end

    context 'when event is for a resource wihtout assignee' do
      it 'returns false' do
        event['object_attributes']['assignee_ids'] = []

        expect(subject.by_resource_assignee?).to be(false)
      end
    end
  end

  describe '#team_member_author?', :clean_cache do
    let(:fixture_path) { 'new_issue.json' }
    let(:user_attrs) { { 'id' => event['object_attributes']['author_id'], 'username' => 'root' } }

    before do
      stub_api_request(path: "/users/#{user_attrs['id']}", response_body: user_attrs)
    end

    context "when author is a team member" do
      it 'returns true' do
        expect(TeamMemberSelectHelper).to receive(:team_member_exist?).with(user_attrs['username']).and_return(true)

        expect(subject.team_member_author?).to be(true)
      end
    end

    context "when author is not a team member" do
      it 'returns false' do
        expect(TeamMemberSelectHelper).to receive(:team_member_exist?).with(user_attrs['username']).and_return(false)

        expect(subject.team_member_author?).to be(false)
      end
    end
  end

  describe '#by_team_member?', :clean_cache do
    let(:fixture_path) { 'new_issue.json' }

    context "when event is made by a team member" do
      it 'returns true' do
        expect(TeamMemberSelectHelper).to receive(:team_member_exist?).with(event['user']['username'], anything).and_return(true)

        expect(subject.by_team_member?).to be(true)
      end
    end

    context "when event is not made by a team member" do
      it 'returns false' do
        expect(TeamMemberSelectHelper).to receive(:team_member_exist?).with(event['user']['username'], anything).and_return(false)

        expect(subject.by_team_member?).to be(false)
      end
    end
  end

  describe '#created_at' do
    it 'returns the resource created_at time' do
      expect(subject.created_at).to eq(Time.parse(event['object_attributes']['created_at']))
    end
  end

  describe '#url' do
    let(:fixture_path) { 'new_issue.json' }

    it 'returns the object_attributes.url' do
      expect(subject.url).to eq(event.dig('object_attributes', 'url'))
    end
  end

  describe '#title' do
    context 'when event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns the issue title' do
        expect(subject.title).to eq(event['object_attributes']['title'])
      end
    end

    context 'when the event is an issue update' do
      let(:fixture_path) { 'update_issue.json' }

      it 'returns the issue title' do
        expect(subject.title).to eq(event['object_attributes']['title'])
      end
    end

    context 'when event is a new merge request' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns the merge request title' do
        expect(subject.title).to eq(event['object_attributes']['title'])
      end
    end
  end

  describe '#description' do
    it 'returns the resource description' do
      expect(subject.description).to eq(event['object_attributes']['description'])
    end

    context 'when the description is absent' do
      it 'returns an empty string' do
        event['object_attributes']['description'] = ''

        expect(subject.description).to be_empty
      end
    end
  end

  describe '#action' do
    it 'returns the resource action' do
      expect(subject.action).to eq(event['object_attributes']['action'])
    end
  end

  describe '#added_label_names' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an array of the issue label names' do
        expect(subject.added_label_names).to match_array(labels)
      end
    end

    context 'when the event is an issue update' do
      let(:fixture_path) { 'update_issue.json' }

      it 'returns an array of added label names' do
        expect(subject.added_label_names).to match_array(current_labels - previous_labels)
      end
    end

    context 'with no label changes' do
      it 'returns an empty array' do
        expect(subject.added_label_names).to eq([])
      end
    end
  end

  describe '#removed_label_names' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an empty array' do
        expect(subject.removed_label_names).to eq([])
      end
    end

    context 'when the event is an issue update' do
      let(:fixture_path) { 'update_issue.json' }

      it 'returns an array of removed label names' do
        expect(subject.removed_label_names).to eq(previous_labels - current_labels)
      end
    end

    context 'when whith no label changes' do
      it 'returns an empty array' do
        expect(subject.removed_label_names).to eq([])
      end
    end
  end

  describe '#label_names' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an array of label names' do
        expect(subject.label_names).to match_array(labels)
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns an array of label names' do
        expect(subject.label_names).to match_array(labels)
      end
    end

    context 'when field labels is absent' do
      let(:fixture_path) { 'new_issue.json' }

      before do
        event.delete('labels')
      end

      it 'returns an empty array' do
        expect(subject.label_names).to eq([])
      end
    end
  end

  describe '#assignee_ids' do
    context 'when the event is a new issue' do
      let(:fixture_path) { 'new_issue.json' }

      it 'returns an array of assignee_ids' do
        expect(subject.assignee_ids).to match_array(event['object_attributes']['assignee_ids'])
      end
    end

    context 'when the event is a new merge request' do
      let(:fixture_path) { 'new_merge_request.json' }

      it 'returns an array of label names' do
        expect(subject.assignee_ids).to match_array(event['object_attributes']['assignee_ids'])
      end
    end
  end

  describe '#type_label' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when labels include type label' do
      before do
        event['labels'] = [{ 'title' => 'type::foo' }]
      end

      it 'returns the type label' do
        expect(subject.type_label).to eq('type::foo')
      end
    end

    context 'when labels does not include type label' do
      before do
        event['labels'] = [{ 'title' => 'bar::foo' }]
      end

      it 'returns nil' do
        expect(subject.type_label).to be_nil
      end
    end
  end

  describe '#type_label_set?' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when labels include type label' do
      before do
        event['labels'] = [{ 'title' => 'type::foo' }]
      end

      it 'returns true' do
        expect(subject).to be_type_label_set
      end
    end

    context 'when labels does not include type label' do
      before do
        event['labels'] = [{ 'title' => 'bar::foo' }]
      end

      it 'returns false' do
        expect(subject).not_to be_type_label_set
      end
    end
  end

  describe '#from_gitlab_org?' do
    let(:fixture_path) { 'note_on_issue.json' }
    let(:sandbox)      { false }

    context 'when project is in the gitlab-org group' do
      before do
        event['project']['path_with_namespace'] = 'gitlab-org/gitlab'
      end

      it 'returns true' do
        expect(subject.from_gitlab_org?(sandbox: sandbox)).to be(true)
      end
    end

    context 'when project is not in the gitlab-org group' do
      before do
        event['project']['path_with_namespace'] = 'gitlab/gitlab-org/gitlab'
      end

      it 'returns false' do
        expect(subject.from_gitlab_org?(sandbox: sandbox)).to be(false)
      end
    end

    context 'when sandbox is enabled' do
      let(:sandbox) { true }

      context 'when the project is triage-ops sandbox' do
        before do
          event['project']['id'] = described_class::TRIAGE_OPS_PLAYGROUND_PROJECT_ID
          event['project']['path_with_namespace'] = 'gitlab-org/quality/engineering-productivity/triage-ops-playground'
        end

        it 'returns true' do
          expect(subject.from_gitlab_org?(sandbox: sandbox)).to be(true)
        end
      end

      context 'when the project is not triage-ops sandbox' do
        before do
          event['project']['id'] = described_class::GITLAB_PROJECT_ID
          event['project']['path_with_namespace'] = 'gitlab-org/gitlab'
        end

        it 'returns false' do
          expect(subject.from_gitlab_org?(sandbox: sandbox)).to be(false)
        end
      end
    end
  end

  describe '#from_gitlab_com?' do
    let(:fixture_path) { 'note_on_issue.json' }
    let(:sandbox)      { false }

    context 'when project is in the gitlab-com group' do
      before do
        event['project']['path_with_namespace'] = 'gitlab-com/gitlab'
      end

      it 'returns true' do
        expect(subject.from_gitlab_com?(sandbox: sandbox)).to be(true)
      end
    end

    context 'when project is not in the gitlab-com group' do
      before do
        event['project']['path_with_namespace'] = 'gitlab/gitlab-com/gitlab'
      end

      it 'returns false' do
        expect(subject.from_gitlab_com?(sandbox: sandbox)).to be(false)
      end
    end

    context 'when sandbox is enabled' do
      let(:sandbox) { true }

      context 'when the project is triage-ops sandbox' do
        before do
          event['project']['id'] = described_class::TRIAGE_OPS_PLAYGROUND_PROJECT_ID
          event['project']['path_with_namespace'] = 'gitlab-org/quality/engineering-productivity/triage-ops-playground'
        end

        it 'returns true' do
          expect(subject.from_gitlab_com?(sandbox: sandbox)).to be(true)
        end
      end

      context 'when the project is not triage-ops sandbox' do
        before do
          event['project']['id'] = described_class::GITLAB_PROJECT_ID
          event['project']['path_with_namespace'] = 'gitlab-com/gitlab'
        end

        it 'returns false' do
          expect(subject.from_gitlab_com?(sandbox: sandbox)).to be(false)
        end
      end
    end
  end

  describe '#from_gitlab_org_security?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project path begins with gitlab-org/security' do
      it 'returns true' do
        event['project']['path_with_namespace'] = 'gitlab-org/security/gitlab'

        expect(subject.from_gitlab_org_security?).to be(true)
      end
    end

    context 'when project path contains but not begins with gitlab-org/security' do
      it 'returns false' do
        event['project']['path_with_namespace'] = 'gitlab/gitlab-org/security/gitlab'

        expect(subject.from_gitlab_org_security?).to be(false)
      end
    end
  end

  describe '#from_gitlab_org_gitlab?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project ID is the gitlab-org/gitlab one' do
      it 'returns true' do
        event['project']['id'] = described_class::GITLAB_PROJECT_ID

        expect(subject.from_gitlab_org_gitlab?).to be(true)
      end
    end

    context 'when project ID is not the gitlab-org/gitlab one' do
      it 'returns false' do
        event['project']['id'] = described_class::GITLAB_PROJECT_ID.succ

        expect(subject.from_gitlab_org_gitlab?).to be(false)
      end
    end
  end

  describe '#from_www_gitlab_com?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project ID is the gitlab-com/www-gitlab-com one' do
      it 'returns true' do
        event['project']['id'] = described_class::WWW_GITLAB_COM_PROJECT_ID

        expect(subject.from_www_gitlab_com?).to be(true)
      end
    end

    context 'when project ID is not the gitlab-com/www-gitlab-com one' do
      it 'returns false' do
        event['project']['id'] = described_class::WWW_GITLAB_COM_PROJECT_ID.succ

        expect(subject.from_www_gitlab_com?).to be(false)
      end
    end
  end

  describe '#from_www_gitlab_com_or_handbook?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project ID is gitlab-com/www-gitlab-com one or gitlab-com/content-sites/handbook or internal-handbook/internal-handbook.gitlab.io' do
      it 'returns true' do
        event['project']['id'] = (described_class::WWW_GITLAB_COM_PROJECT_ID || described_class::CONTENT_SITES_HANDBOOK_PROJECT_ID || described_class::INTERNAL_HANDBOOK_PROJECT_ID)

        expect(subject.from_www_gitlab_com_or_handbook?).to be(true)
      end
    end

    context 'when project ID is not gitlab-com/www-gitlab-com one and gitlab-com/content-sites/handbook and internal-handbook/internal-handbook.gitlab.io' do
      it 'returns false' do
        event['project']['id'] = (described_class::WWW_GITLAB_COM_PROJECT_ID.succ && described_class::CONTENT_SITES_HANDBOOK_PROJECT_ID.succ && described_class::INTERNAL_HANDBOOK_PROJECT_ID.succ)

        expect(subject.from_www_gitlab_com_or_handbook?).to be(false)
      end
    end
  end

  describe '#from_runbooks?' do
    let(:fixture_path) { 'note_on_issue.json' }

    context 'when project ID is the gitlab-com/runbooks one' do
      it 'returns true' do
        event['project']['id'] = described_class::RUNBOOKS_PROJECT_ID

        expect(subject.from_runbooks?).to be(true)
      end
    end

    context 'when project ID is not the gitlab-com/runbooks one' do
      it 'returns false' do
        event['project']['id'] = described_class::RUNBOOKS_PROJECT_ID.succ

        expect(subject.from_runbooks?).to be(false)
      end
    end
  end

  describe '#from_part_of_product_project?' do
    let(:fixture_path) { 'new_issue.json' }
    let(:csv_row) { "#{event['project']['path_with_namespace']},#{event['project']['id']}" }

    before do
      stub_request(:get, Triage::PartOfProductProjects::CSV_URLS[:com])
        .to_return(body: "project_path,project_id\n#{csv_row}")
    end

    context 'when project is in the "part of product" list' do
      it 'returns true' do
        expect(subject.from_part_of_product_project?).to be(true)
      end
    end

    context 'when project ID is not in the "part of product" list' do
      it 'returns false' do
        event['project']['id'] = event['project']['id'] + 1

        expect(subject.from_part_of_product_project?).to be(false)
      end
    end
  end

  describe 'from_master_broken_incidents_project?' do
    context 'when project ID is equal to MASTER_BROKEN_INCIDENT_PROJECT_ID' do
      it 'returns true' do
        event['project']['id'] = described_class::MASTER_BROKEN_INCIDENT_PROJECT_ID

        expect(subject.from_master_broken_incidents_project?).to be(true)
      end
    end

    context 'when project ID is not equal to MASTER_BROKEN_INCIDENT_PROJECT_ID' do
      it 'returns false' do
        event['project']['id'] = described_class::MASTER_BROKEN_INCIDENT_PROJECT_ID.succ

        expect(subject.from_master_broken_incidents_project?).to be(false)
      end
    end
  end

  describe '#automation_author?' do
    let(:fixture_path) { 'new_issue.json' }

    described_class::AUTOMATION_IDS.each do |id|
      context "when author id is '#{id}'" do
        it 'returns true' do
          event['object_attributes']['author_id'] = id

          expect(subject.automation_author?).to be(true)
        end
      end
    end

    context "when username is 42" do
      it 'returns false' do
        event['object_attributes']['author_id'] = 42

        expect(subject.automation_author?).to be(false)
      end
    end
  end

  describe '#gitlab_bot_event_actor?' do
    let(:fixture_path) { 'new_issue.json' }

    context 'when actor username is gitlab-bot' do
      it 'returns true' do
        event['user'] = { 'username' => 'gitlab-bot' }

        expect(subject.gitlab_bot_event_actor?).to be(true)
      end
    end

    context 'when actor username is something else' do
      it 'returns false' do
        event['user'] = { 'username' => 'nice_author' }

        expect(subject.gitlab_bot_event_actor?).to be(false)
      end
    end
  end

  describe '#author_is_gitlab_service_account?' do
    let(:fixture_path) { 'new_issue.json' }

    before do
      allow(subject).to receive(:resource_author).and_return(Triage::User.new(username: username))
    end

    context "when user username is a project service account" do
      let(:username) { 'project_278964_bot4' }

      it 'returns true' do
        expect(subject.author_is_gitlab_service_account?).to be(true)
      end
    end

    context "when user username is a group service account" do
      let(:username) { 'group_7137538_bot_97d0ed2e6bb4ab962e3a43a628c06974' }

      it 'returns true' do
        expect(subject.author_is_gitlab_service_account?).to be(true)
      end
    end

    context "when user username is foo" do
      let(:username) { 'foo' }

      it 'returns false' do
        expect(subject.author_is_gitlab_service_account?).to be(false)
      end
    end
  end

  describe '#wider_community_author?' do
    let(:fixture_path) { 'new_issue.json' }

    before do
      allow(subject).to receive(:automation_author?).and_return(false)
      allow(subject).to receive(:author_is_gitlab_service_account?).and_return(false)
      allow(subject).to receive(:gitlab_contractor?).and_return(false)
      allow(subject).to receive(:team_member_author?).and_return(false)
    end

    it 'returns true' do
      expect(subject.wider_community_author?).to be(true)
    end

    context "when author is an automation user" do
      before do
        allow(subject).to receive(:automation_author?).and_return(true)
      end

      it 'returns false' do
        expect(subject.wider_community_author?).to be(false)
      end
    end

    context "when user is a gitlab service account" do
      before do
        allow(subject).to receive(:author_is_gitlab_service_account?).and_return(true)
      end

      it 'returns false' do
        expect(subject.wider_community_author?).to be(false)
      end
    end

    context "when user is a gitlab contractor account" do
      before do
        allow(subject).to receive(:gitlab_contractor?).and_return(true)
      end

      it 'returns false' do
        expect(subject.wider_community_author?).to be(false)
      end
    end

    context "when author is a team member" do
      it 'returns false' do
        expect(subject).to receive(:team_member_author?).and_return(true)

        expect(subject.wider_community_author?).to be(false)
      end
    end

    context "when project is private" do
      it 'returns false' do
        expect(subject).to receive(:project_public?).and_return(false)

        expect(subject.wider_community_author?).to be(false)
      end
    end
  end

  describe '#gitlab_contractor?', :clean_cache do
    let(:fixture_path) { 'new_merge_request.json' }

    context 'when author is a gitlab contractors group member' do
      before do
        allow(Triage).to receive(:gitlab_org_contractors_member_ids)
          .and_return([event.dig('object_attributes', 'author_id')])
      end

      it 'returns true' do
        expect(subject.gitlab_contractor?).to be(true)
      end
    end

    context 'when author is not a gitlab contractors group member' do
      before do
        allow(Triage).to receive(:gitlab_org_contractors_member_ids).and_return([])
      end

      it 'returns false' do
        expect(subject.gitlab_contractor?).to be(false)
      end
    end
  end

  describe '#jihu_contributor?', :clean_cache do
    let(:fixture_path) { 'new_merge_request.json' }

    context 'when author is a JiHu team member' do
      before do
        allow(Triage).to receive(:jihu_team_member_ids)
          .and_return([event.dig('object_attributes', 'author_id')])
      end

      it 'returns true' do
        expect(subject.jihu_contributor?).to be(true)
      end
    end

    context 'when author is not a JiHu team member' do
      before do
        allow(Triage).to receive(:jihu_team_member_ids).and_return([])
      end

      it 'returns false' do
        expect(subject.jihu_contributor?).to be(false)
      end
    end
  end

  describe '#project_id' do
    it 'returns the project id' do
      expect(subject.project_id).to eq(event['project']['id'])
    end
  end

  describe '#project_web_url' do
    it 'returns the project id' do
      expect(subject.project_web_url).to eq(event['project']['web_url'])
    end
  end

  describe '#with_project_id?' do
    context 'when project id matches' do
      it 'returns true' do
        expect(subject.with_project_id?(event['project']['id'])).to be(true)
      end
    end

    context 'when project id does not match' do
      it 'returns false' do
        expect(subject.with_project_id?(42)).to be(false)
      end
    end
  end

  describe '#project_path_with_namespace' do
    it 'returns the project path with namespace' do
      expect(subject.project_path_with_namespace).to eq(event['project']['path_with_namespace'])
    end
  end

  describe '#milestone_id' do
    let(:fixture_path) { 'update_issue.json' }

    it 'returns the milestone_id' do
      expect(subject.milestone_id).to eq(event.dig('object_attributes', 'milestone_id'))
    end
  end

  describe '#added_milestone_id' do
    let(:fixture_path) { 'update_issue.json' }

    context 'when event did not change milestone id' do
      it 'returns nil' do
        expect(subject.added_milestone_id).to be_nil
      end
    end

    context 'when event changed milestone id' do
      let(:expected_milestone_id) { 105 }

      before do
        event['changes'].merge!(
          'milestone_id' => { 'previous' => nil, 'current' => expected_milestone_id }
        )
      end

      it 'returns the current milestone_id from changes' do
        expect(subject.added_milestone_id).to eq(expected_milestone_id)
      end
    end
  end

  shared_examples 'a Triage::IssuableEvent' do
    describe '#iid' do
      it 'returns the issue iid' do
        expect(subject.iid).to eq(event.dig('object_attributes', 'iid'))
      end
    end

    describe '#resource_open?' do
      context 'when the resource is opened' do
        it 'returns true' do
          expect(subject).to be_resource_open
        end
      end

      %w[closed merged].each do |mr_state|
        context "when the resource is #{mr_state}" do
          it 'returns false' do
            event['object_attributes']['state'] = mr_state

            expect(subject).not_to be_resource_open
          end
        end
      end
    end
  end

  describe Triage::IssueEvent do
    let(:fixture_path) { 'new_issue.json' }

    it_behaves_like 'a Triage::IssuableEvent'

    describe '#noteable_path' do
      it 'returns the API URL for the issue' do
        expect(subject.noteable_path)
          .to match(%r{\A/projects/\d+/issues/\d+\z})
      end
    end

    describe '#weight' do
      it 'returns the weight in the issue' do
        expect(subject.weight).to eq(event['object_attributes']['weight'])
      end
    end

    describe '#project_public?' do
      context 'when project visibility level is 20' do
        it 'returns true' do
          event['project']['visibility_level'] = 20

          expect(subject.project_public?).to be_truthy
        end
      end

      context 'when project visibility level is not 20' do
        it 'returns true' do
          event['project']['visibility_level'] = 0

          expect(subject.project_public?).to be_falsey
        end
      end
    end

    describe '#project_internal?' do
      context 'when project visibility level is 10' do
        it 'returns true' do
          event['project']['visibility_level'] = 10

          expect(subject.project_internal?).to be_truthy
        end
      end

      context 'when project visibility level is not 10' do
        it 'returns true' do
          event['project']['visibility_level'] = 0

          expect(subject.project_internal?).to be_falsey
        end
      end
    end

    describe '#project_private?' do
      context 'when project visibility level is 0' do
        it 'returns true' do
          event['project']['visibility_level'] = 0

          expect(subject.project_private?).to be_truthy
        end
      end

      context 'when project visibility level is not 0' do
        it 'returns true' do
          event['project']['visibility_level'] = 10

          expect(subject.project_private?).to be_falsey
        end
      end
    end
  end

  describe Triage::IncidentEvent do
    let(:fixture_path) { 'new_incident.json' }

    describe '#noteable_path' do
      it 'returns the API URL for the incident' do
        expect(subject.noteable_path)
          .to match(%r{\A/projects/\d+/issues/\d+\z})
      end
    end
  end

  describe Triage::MergeRequestEvent do
    let(:fixture_path) { 'new_merge_request.json' }

    it_behaves_like 'a Triage::IssuableEvent'

    describe '#source_branch' do
      it 'returns work_in_progress from the event' do
        expect(subject.source_branch).to eq(event['object_attributes']['source_branch'])
      end
    end

    describe '#wip?' do
      it 'returns work_in_progress from the event' do
        expect(subject.wip?).to eq(event['object_attributes']['work_in_progress'])
      end
    end

    describe '#noteable_path' do
      it 'returns the API URL for the merge request' do
        expect(subject.noteable_path)
          .to match(%r{\A/projects/\d+/merge_requests/\d+\z})
      end
    end

    describe '#merge_event?' do
      it 'returns true if it action is merge' do
        event['object_attributes']['action'] = 'merge'

        expect(subject.merge_event?).to be(true)
      end

      it 'returns false if action is not merge' do
        event['object_attributes']['action'] = 'open'

        expect(subject.merge_event?).to be(false)
      end
    end

    describe '#revision_update?' do
      it 'returns true if action is update and it has oldrev' do
        event['object_attributes']['action'] = 'update'
        event['object_attributes']['oldrev'] = 'somerevision'

        expect(subject.revision_update?).to be(true)
      end

      it 'returns false if action is open' do
        event['object_attributes']['action'] = 'open'

        expect(subject.revision_update?).to be(false)
      end
    end

    describe '#approval_event?' do
      it 'returns true if the action is approval' do
        event['object_attributes']['action'] = 'approval'

        expect(subject.approval_event?).to be(true)
      end

      it 'returns false if action is not approval' do
        event['object_attributes']['action'] = 'approved'

        expect(subject.approval_event?).to be(false)
      end
    end

    describe '#approved_event?' do
      it 'returns true if the action is approved' do
        event['object_attributes']['action'] = 'approved'

        expect(subject.approved_event?).to be(true)
      end

      it 'returns false if action is not approved' do
        event['object_attributes']['action'] = 'approval'

        expect(subject.approved_event?).to be(false)
      end
    end

    describe '#source_branch_is?' do
      it 'returns true if the source_branch matches' do
        event['object_attributes']['source_branch'] = '14-9-stable-ee'

        expect(subject.source_branch_is?('14-9-stable-ee')).to be(true)
      end

      it 'returns false if the source_branch does not match' do
        event['object_attributes']['source_branch'] = 'master'

        expect(subject.source_branch_is?('14-9-stable-ee')).to be(false)
      end
    end

    describe '#target_branch_is_main_or_master?' do
      it 'returns true if the target branch is main' do
        event['object_attributes']['target_branch'] = 'main'

        expect(subject.target_branch_is_main_or_master?).to be(true)
      end

      it 'returns true if the target branch is master' do
        event['object_attributes']['target_branch'] = 'master'

        expect(subject.target_branch_is_main_or_master?).to be(true)
      end

      it 'returns false if the target branch is develop' do
        event['object_attributes']['target_branch'] = 'develop'

        expect(subject.target_branch_is_main_or_master?).to be(false)
      end
    end

    describe '#target_branch_is_stable_branch?' do
      it 'returns true if the target_branch has the stable-ee suffix' do
        event['object_attributes']['target_branch'] = '14-9-stable-ee'

        expect(subject.target_branch_is_stable_branch?).to be(true)
      end

      it 'returns false if the target_branch does not have the stable-ee suffix' do
        event['object_attributes']['target_branch'] = 'master'

        expect(subject.target_branch_is_stable_branch?).to be(false)
      end
    end

    describe '#last_commit_sha' do
      it 'returns SHA if last commit ID is present' do
        expect(subject.last_commit_sha).to eq('58b824444e5b8fbcc0376d7dd37ed6684291f278')
      end

      it 'returns nil if last commit ID is not present' do
        event['object_attributes']['last_commit'] = nil

        expect(subject.last_commit_sha).to be_nil
      end
    end

    describe '#revert_mr?' do
      context 'when the MR title starts with "revert"' do
        before do
          event['object_attributes']['title'] = 'revert - an MR title'
        end

        it { expect(subject.revert_mr?).to be_truthy }
      end

      context 'when the MR title starts with "Revert"' do
        before do
          event['object_attributes']['title'] = 'Revert: An MR title'
        end

        it { expect(subject.revert_mr?).to be_truthy }
      end

      context 'when the MR title does not start with "Revert" or "revert"' do
        before do
          event['object_attributes']['title'] = 'A non-revert MR title'
        end

        it { expect(subject.revert_mr?).to be_falsey }
      end
    end

    describe '#project_public?' do
      context 'when project visibility level is 20' do
        it 'returns true' do
          event['object_attributes']['target']['visibility_level'] = 20

          expect(subject.project_public?).to be_truthy
        end
      end

      context 'when project visibility level is not 20' do
        it 'returns true' do
          event['object_attributes']['target']['visibility_level'] = 0

          expect(subject.project_public?).to be_falsey
        end
      end
    end

    describe '#project_internal?' do
      context 'when project visibility level is 10' do
        it 'returns true' do
          event['object_attributes']['target']['visibility_level'] = 10

          expect(subject.project_internal?).to be_truthy
        end
      end

      context 'when project visibility level is not 10' do
        it 'returns true' do
          event['object_attributes']['target']['visibility_level'] = 0

          expect(subject.project_internal?).to be_falsey
        end
      end
    end

    describe '#project_private?' do
      context 'when project visibility level is 0' do
        it 'returns true' do
          event['object_attributes']['target']['visibility_level'] = 0

          expect(subject.project_private?).to be_truthy
        end
      end

      context 'when project visibility level is not 0' do
        it 'returns true' do
          event['object_attributes']['target']['visibility_level'] = 20

          expect(subject.project_private?).to be_falsey
        end
      end
    end

    describe '#from_community_fork?' do
      let(:source_path_with_namespace) { nil }

      before do
        event['object_attributes']['source']['path_with_namespace'] = source_path_with_namespace
      end

      context 'when the MR is from gitlab-community/' do
        let(:source_path_with_namespace) { 'gitlab-community/gitlab' }

        it { expect(subject.from_community_fork?).to be_truthy }
      end

      context 'when the MR is from gitlab-jh/' do
        let(:source_path_with_namespace) { 'gitlab-jh/jh-team/gitlab' }

        it { expect(subject.from_community_fork?).to be_truthy }
      end

      context 'when the MR is not from a community fork' do
        it { expect(subject.from_community_fork?).to be_falsey }
      end
    end

    describe '#from_security_fork?' do
      let(:source_path_with_namespace) { nil }

      before do
        event['object_attributes']['source']['path_with_namespace'] = source_path_with_namespace
      end

      context 'when the MR is from gitlab-org/security/' do
        let(:source_path_with_namespace) { 'gitlab-org/security/gitlab' }

        it { expect(subject.from_security_fork?).to be_truthy }
      end

      context 'when the MR is from gitlab-community/' do
        let(:source_path_with_namespace) { 'gitlab-community/gitlab' }

        it { expect(subject.from_security_fork?).to be_falsey }
      end

      context 'when the MR is from gitlab-jh/' do
        let(:source_path_with_namespace) { 'gitlab-jh/jh-team/gitlab' }

        it { expect(subject.from_security_fork?).to be_falsey }
      end

      context 'when the MR is not from a security fork' do
        it { expect(subject.from_security_fork?).to be_falsey }
      end
    end

    describe '#first_contribution?' do
      let(:first_contribution_field) { nil }

      before do
        event['object_attributes']['first_contribution'] = first_contribution_field
      end

      context 'when the MR is the first contribution from a user' do
        let(:first_contribution_field) { true }

        it { expect(subject.first_contribution?).to be_truthy }
      end

      context 'when the MR is not the first contribution from a user' do
        let(:first_contribution_field) { false }

        it { expect(subject.first_contribution?).to be_falsey }
      end
    end

    describe '#resource_merged?' do
      context 'when the MR is merged' do
        before do
          event['object_attributes']['state'] = 'merged'
        end

        it { expect(subject.resource_merged?).to be_truthy }
      end

      context 'when the MR is not merged' do
        it { expect(subject.resource_merged?).to be_falsey }
      end
    end
  end

  describe Triage::NoteEvent do
    shared_examples 'a Triage::NoteEvent' do |resource_type|
      describe '#iid' do
        it 'returns the issue iid' do
          expect(subject.iid).to eq(event.dig(resource_type, 'iid'))
        end
      end

      describe '#resource_open?' do
        context 'when the noteable is opened' do
          it 'returns true' do
            expect(subject).to be_resource_open
          end
        end

        %w[closed merged].each do |mr_state|
          context "when the noteable is #{mr_state}" do
            it 'returns false' do
              event[resource_type]['state'] = mr_state

              expect(subject).not_to be_resource_open
            end
          end
        end
      end

      describe '#key' do
        context "when the event is an #{resource_type} note" do
          it "returns #{resource_type}.note" do
            expect(subject.key).to eq("#{resource_type}.note")
          end
        end
      end

      describe '#created_at' do
        it 'returns the note created_at time' do
          expect(subject.created_at).to eq(Time.parse(event['object_attributes']['created_at']))
        end
      end

      describe '#new_comment' do
        it 'returns the new note content' do
          expect(subject.new_comment).to eq(event['object_attributes']['description'])
        end
      end

      describe '#resource_author_id' do
        it 'returns the noteable author id' do
          expect(subject.resource_author_id).to eq(event[resource_type]['author_id'])
        end
      end

      describe '#noteable_author' do
        let(:user_attrs) { { 'id' => event[resource_type]['author_id'], 'username' => 'root' } }

        it 'returns a User object that corresponds to the object_attributes.author_id' do
          expect_api_request(path: "/users/#{user_attrs['id']}", response_body: user_attrs) do
            expect(subject.noteable_author).to be_a(Triage::User)
            expect(subject.noteable_author.to_h).to eq(user_attrs)
            expect(subject.noteable_author.id).to eq(user_attrs['id'])
            expect(subject.noteable_author.username).to eq(user_attrs['username'])
          end
        end
      end

      describe '#by_resource_author?' do
        let(:resource_author_id) { 1 }

        context 'when note is created by the noteable author' do
          it 'returns true' do
            event[resource_type]['author_id'] = event['user']['id']

            expect(subject.by_resource_author?).to be(true)
          end
        end

        context 'when note is not created by the noteable author' do
          it 'returns false' do
            event[resource_type]['author_id'] = event['user']['id'] + 1

            expect(subject.by_resource_author?).to be(false)
          end
        end
      end

      describe '#by_resource_assignee?' do
        context 'when note is created by the noteable assignee' do
          it 'returns true' do
            event[resource_type]['assignee_ids'] = [event['user']['id']]

            expect(subject.by_resource_assignee?).to be(true)
          end
        end

        context 'when note is not created by the noteable assignee' do
          it 'returns false' do
            event[resource_type]['assignee_ids'] = [event['user']['id'] + 1]

            expect(subject.by_resource_assignee?).to be(false)
          end
        end

        context 'when note is created for noteable without assignee' do
          it 'returns false' do
            event[resource_type]['assignee_ids'] = []

            expect(subject.by_resource_assignee?).to be(false)
          end
        end
      end
    end

    context 'when the note is on an issue' do
      let(:fixture_path) { 'note_on_issue.json' }

      it_behaves_like 'a Triage::NoteEvent', 'issue'

      describe '#id' do
        it 'returns the note id' do
          expect(subject.id).to eq(826963813)
        end
      end

      describe '#label_names' do
        let(:labels) { event.dig('issue', 'labels').map { |label| label['title'] } }

        it 'returns an array of label names' do
          expect(subject.label_names).to match_array(labels)
        end
      end

      describe '#assignee_ids' do
        let(:assignee_ids) { event.dig('issue', 'assignee_ids') }

        it 'returns an array of assignee_ids' do
          expect(subject.assignee_ids).to match_array(assignee_ids)
        end
      end

      describe '#noteable_path' do
        it 'returns the API URL for the issue' do
          expect(subject.noteable_path)
            .to match(%r{\A/projects/\d+/issues/\d+\z})
        end
      end

      describe '#note_on_issue?' do
        it 'returns true' do
          expect(subject.note_on_issue?).to be(true)
        end
      end

      describe '#note_on_merge_request?' do
        it 'returns true' do
          expect(subject.note_on_merge_request?).to be(false)
        end
      end

      describe '#title' do
        it 'returns the issue title' do
          expect(subject.title).to be(event.dig('issue', 'title'))
        end
      end

      describe '#project_public?' do
        context 'when project visibility level is 20' do
          it 'returns true' do
            event['project']['visibility_level'] = 20

            expect(subject.project_public?).to be_truthy
          end
        end

        context 'when project visibility level is not 20' do
          it 'returns true' do
            event['project']['visibility_level'] = 0

            expect(subject.project_public?).to be_falsey
          end
        end
      end

      describe '#project_internal?' do
        context 'when project visibility level is 10' do
          it 'returns true' do
            event['project']['visibility_level'] = 10

            expect(subject.project_internal?).to be_truthy
          end
        end

        context 'when project visibility level is not 10' do
          it 'returns true' do
            event['project']['visibility_level'] = 0

            expect(subject.project_internal?).to be_falsey
          end
        end
      end

      describe '#project_private?' do
        context 'when project visibility level is 0' do
          it 'returns true' do
            event['project']['visibility_level'] = 0

            expect(subject.project_private?).to be_truthy
          end
        end

        context 'when project visibility level is not 0' do
          it 'returns true' do
            event['project']['visibility_level'] = 20

            expect(subject.project_private?).to be_falsey
          end
        end
      end

      describe '#confidential?' do
        context 'when event_type is `note`' do
          it 'returns false' do
            event['event_type'] = 'note'

            expect(subject.confidential?).to be_falsey
          end
        end

        context 'when event_type is `confidential_note`' do
          it 'returns true' do
            event['event_type'] = 'confidential_note'

            expect(subject.confidential?).to be_truthy
          end
        end
      end
    end

    context 'when the note is on a merge request' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      it_behaves_like 'a Triage::NoteEvent', 'merge_request'

      describe '#id' do
        it 'returns the note id' do
          expect(subject.id).to eq(826964861)
        end
      end

      describe '#label_names' do
        let(:labels) { event.dig('merge_request', 'labels').map { |label| label['title'] } }

        it 'returns an array of label names' do
          expect(subject.label_names).to match_array(labels)
        end
      end

      describe '#assignee_ids' do
        let(:assignee_ids) { event.dig('merge_request', 'assignee_ids') }

        it 'returns an array of assignee_ids' do
          expect(subject.assignee_ids).to match_array(assignee_ids)
        end
      end

      describe '#noteable_path' do
        it 'returns the API URL for the merge request' do
          expect(subject.noteable_path)
            .to match(%r{\A/projects/\d+/merge_requests/\d+\z})
        end
      end

      describe '#note_on_issue?' do
        it 'returns true' do
          expect(subject.note_on_issue?).to be(false)
        end
      end

      describe '#note_on_merge_request?' do
        it 'returns true' do
          expect(subject.note_on_merge_request?).to be(true)
        end
      end

      describe '#title' do
        it 'returns the merge request title' do
          expect(subject.title).to be(event.dig('merge_request', 'title'))
        end
      end

      describe '#project_public?' do
        context 'when project visibility level is 20' do
          it 'returns true' do
            event['merge_request']['target']['visibility_level'] = 20

            expect(subject.project_public?).to be_truthy
          end
        end

        context 'when project visibility level is not 20' do
          it 'returns true' do
            event['merge_request']['target']['visibility_level'] = 0

            expect(subject.project_public?).to be_falsey
          end
        end
      end

      describe '#project_internal?' do
        context 'when project visibility level is 10' do
          it 'returns true' do
            event['merge_request']['target']['visibility_level'] = 10

            expect(subject.project_internal?).to be_truthy
          end
        end

        context 'when project visibility level is not 10' do
          it 'returns true' do
            event['merge_request']['target']['visibility_level'] = 0

            expect(subject.project_internal?).to be_falsey
          end
        end
      end

      describe '#project_private?' do
        context 'when project visibility level is 0' do
          it 'returns true' do
            event['merge_request']['target']['visibility_level'] = 0

            expect(subject.project_private?).to be_truthy
          end
        end

        context 'when project visibility level is not 0' do
          it 'returns true' do
            event['merge_request']['target']['visibility_level'] = 20

            expect(subject.project_private?).to be_falsey
          end
        end
      end
    end

    context 'when the note is on an unknown noteable_type' do
      let(:fixture_path) { 'note_on_merge_request.json' }

      before do
        event['object_attributes']['noteable_type'] = 'foo'
      end

      describe '#label_names' do
        it 'returns an array of label names' do
          expect { subject.label_names }.to raise_error(described_class::UnknownNoteableTypeError, "`foo` is an unknown noteable_type!")
        end
      end

      describe '#assignee_ids' do
        it 'raises UnknownNoteableTypeError' do
          expect { subject.assignee_ids }.to raise_error(described_class::UnknownNoteableTypeError, "`foo` is an unknown noteable_type!")
        end
      end
    end
  end

  describe Triage::PipelineEvent do
    let(:fixture_path) { 'pipeline_failed.json' }

    describe '#key' do
      it "returns pipeline.failed" do
        expect(subject.key).to eq("pipeline.failed")
      end
    end

    describe '#id' do
      it 'returns the pipeline id' do
        expect(subject.id).to eq(event['object_attributes']['id'])
      end
    end

    describe '#ref' do
      it 'returns the pipeline ref' do
        expect(subject.ref).to eq(event['object_attributes']['ref'])
      end
    end

    describe '#sha' do
      it 'returns the pipeline sha' do
        expect(subject.sha).to eq(event['object_attributes']['sha'])
      end
    end

    describe '#source' do
      it 'returns the pipeline source' do
        expect(subject.source).to eq(event['object_attributes']['source'])
      end
    end

    describe '#status' do
      it 'returns the pipeline status' do
        expect(subject.status).to eq(event['object_attributes']['status'])
      end
    end

    describe '#variables' do
      it 'returns the pipeline variables' do
        expect(subject.variables).to eq(event['object_attributes']['variables'])
      end
    end

    describe '#builds' do
      it 'returns the pipeline builds' do
        expect(subject.builds).to eq(event['builds'])
      end
    end

    describe '#source_job_id' do
      context 'when source_pipeline is not there' do
        it 'returns the pipeline source_pipeline.job_id' do
          expect(subject.source_job_id).to be_nil
        end
      end

      context 'when source_pipeline is not nil' do
        before do
          event['source_pipeline'] = { 'job_id' => 42 }
        end

        it 'returns the pipeline source_pipeline.job_id' do
          expect(subject.source_job_id).to eq(42)
        end
      end
    end

    describe '#web_url' do
      it 'returns the constructed pipeline web url' do
        expect(subject.web_url).to eq("#{subject.project_web_url}/-/pipelines/#{event['object_attributes']['id']}")
      end
    end

    describe '#commit_header' do
      it 'returns the commit header' do
        expect(subject.commit_header).to eq(Triage::Commit.new(event['commit']).header)
      end
    end

    describe '#merge_request_pipeline?' do
      context 'when merge request payload has data' do
        it 'return true' do
          expect(subject.merge_request_pipeline?).to be true
        end
      end

      context 'when merge request payload is nil' do
        let(:event) do
          JSON.parse(read_fixture("/reactive/#{fixture_path}")).merge({ 'merge_request' => nil })
        end

        it 'return false' do
          expect(subject.merge_request_pipeline?).to be false
        end
      end
    end

    describe '#merge_request' do
      context 'when pipeline event has merge_request data' do
        it 'returns a merge request opened for the pipeline SHA' do
          expect(subject.merge_request).to be_a(Triage::MergeRequest)
          expect(subject.merge_request.title).to eq('Test')
        end
      end

      context 'when pipeline event does not have merge_request data' do
        let(:merge_request_attrs) do
          JSON.parse(read_fixture("/reactive/new_merge_request.json"))['object_attributes']
        end

        before do
          event.delete('merge_request')
        end

        it 'returns a merge request opened for the pipeline SHA' do
          expect_api_request(path: "/projects/#{subject.project_id}/repository/commits/#{subject.sha}/merge_requests", response_body: [merge_request_attrs]) do
            expect(subject.merge_request).to be_a(Triage::MergeRequest)
            expect(subject.merge_request.title).to eq('Disable metadata creation hack in blob service tests')
          end
        end
      end
    end
  end
end
