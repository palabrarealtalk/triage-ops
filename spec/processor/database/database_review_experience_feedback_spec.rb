# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/database/database_review_experience_feedback'

RSpec.describe Triage::DatabaseReviewExperienceFeedback do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request'
      }
    end
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge', 'merge_request.close']

  describe '#applicable?' do
    context 'when the merge request is labeled as database::approved' do
      let(:label_names) { %w[database::approved] }

      include_examples 'event is applicable'
    end

    context 'when the merge request is labeled as database::reviewed' do
      let(:label_names) { %w[database::reviewed] }

      include_examples 'event is applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end

    context 'when the merge request is labeled as spam' do
      let(:label_names) { %w[Spam] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts code review experience message' do
      body = add_automation_suffix('processor/database/database_review_experience_feedback.rb') do
        <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          Hello @#{event.resource_author.username} :wave:

          The database team is looking for ways to improve the database review
          process and we would love your help!

          If you'd be open to someone on the database team reaching out to you
          for a chat, or if you'd like to leave some feedback asynchronously,
          just post a reply to this comment mentioning:

          ```
          @gitlab-org/database-team
          ```

          And someone will be by shortly!

          Thanks for your help! :heart:
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
