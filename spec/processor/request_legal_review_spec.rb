# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/processor/request_legal_review'
require_relative '../../triage/triage/event'

RSpec.describe Triage::RequestLegalReview do
  let(:project_web_url) { 'https://gitlab.example/group/project' }

  let(:event_attr_base) do
    {
      object_kind: 'issue',
      action: 'open',
      title: 'KPI issue for 1st quarter',
      from_part_of_product_project?: true,
      project_web_url: project_web_url
    }
  end

  let(:required_feature_labels) { ['type::feature', 'feature::addition'] }

  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) { event_attr_base }
    let(:label_names) { [*required_feature_labels, 'workflow::ready for development'] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.open', 'issue.update', 'issue.reopen']

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when event project is not from part of product project' do
      let(:event_attrs) { event_attr_base.merge(from_part_of_product_project?: false) }

      include_examples 'event is not applicable'
    end

    context 'with no feature labels' do
      let(:label_names) { ['type::bug', 'bug::availability'] }

      include_examples 'event is not applicable'
    end

    context 'with feature::removal label' do
      let(:label_names) { ['type::feature', 'feature::removal'] }

      include_examples 'event is not applicable'
    end

    described_class::APPLICABLE_WORKFLOW_LABELS.each do |workflow_label|
      context "with workflow label #{workflow_label}" do
        let(:label_names) { [*required_feature_labels, workflow_label] }

        include_examples 'event is applicable'
      end
    end

    context 'when issue already has the legal label applied' do
      let(:label_names) { [*required_feature_labels, 'workflow::ready for development', described_class::LEGAL_LABEL] }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    let(:comment_body)            { "/label ~\"#{described_class::LEGAL_LABEL}\"" }
    let(:create_legal_issue_path) { "/projects/#{described_class::LEGAL_COMPLIANCE_PROJECT_ID}/issues" }
    let(:event_issue_notes_path)  { "/projects/#{event.project_id}/issues/#{event.iid}/notes" }
    let(:created_issue_title)     { 'Legal Review Questionnaire: KPI issue for 1st quarter' }
    let(:issue_payload) do
      {
        title: created_issue_title,
        description: subject.__send__(:legal_review_issue_description)
      }
    end

    it 'builds the correct issue payload' do
      expect(issue_payload[:title]).to eq(created_issue_title)
      expect(issue_payload[:description]).to include('/label ~"Product Dev - Legal Review Questionnaire"')
      expect(issue_payload[:description]).to include(project_web_url)
    end

    it 'creates an issue and posts a comment' do
      expect_api_requests do |requests|
        requests << stub_api_request(verb: :post, path: create_legal_issue_path, request_body: issue_payload)
        requests << stub_api_request(verb: :post, path: event_issue_notes_path, request_body: { body: comment_body })

        subject.process
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end
end
