# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/docs_only_labeller'

RSpec.describe Triage::DocsOnlyLabeller do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:resource_open) { true }
    let(:event_attrs) { { resource_open?: resource_open } }
  end

  include_examples 'registers listeners', ['merge_request.update']

  subject { described_class.new(event) }

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when the merge request is not open' do
      let(:resource_open) { false }

      include_examples 'event is not applicable'
    end

    context 'when the merge request is not from a docs project' do
      let(:project_id) { 1 }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'schedules DocsOnlyLabellerJob' do
      expect(Triage::DocsOnlyLabellerJob).to receive(:perform_in).with(described_class::FIVE_MINUTES, event)

      subject.process
    end
  end
end
