# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/ceo_chief_of_staff_team_ops_notifier'
require_relative '../../triage/triage/event'

RSpec.describe Triage::CeoChiefOfStaffTeamOpsNotifier do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        project_id: project_id,
        iid: merge_request_iid,
        from_gitlab_org?: true
      }
    end
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "something/old.md",
          "new_path" => "something/new.md"
        }
      ]
    }
  end

  include_context 'with merge request notes'

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update', 'merge_request.open']

  describe '#applicable?' do
    context 'when relevant files are changed but event project is not gitlab-com/www-gitlab-com or gitlab-com/content-sites/handbook or internal-handbook/internal-handbook.gitlab.io' do
      let(:project_id) { 12345 }
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => ".gitlab/CODEOWNERS",
              "new_path" => "something/new.md"
            }
          ]
        }
      end

      before do
        allow(event).to receive(:from_www_gitlab_com_or_handbook?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event project is gitlab-com/www-gitlab-com' do
      let(:project_id) { Triage::Event::WWW_GITLAB_COM_PROJECT_ID }

      before do
        allow(event).to receive(:from_www_gitlab_com_or_handbook?).and_return(true)
      end

      context 'when there was no change to relevant files' do
        include_examples 'event is not applicable'
      end

      context 'when there is already a comment for the same purpose' do
        let(:merge_request_changes) do
          {
            'changes' => [
              {
                "old_path" => ".gitlab/CODEOWNERS",
                "new_path" => "something/new.md"
              }
            ]
          }
        end

        let(:merge_request_notes) do
          [
            { body: 'review comment 1' },
            { body: comment_mark }
          ]
        end

        include_examples 'event is not applicable'
      end

      context 'when there were changes to relevant files' do
        let(:merge_request_changes) do
          {
            'changes' => [
              {
                "old_path" => ".gitlab/CODEOWNERS",
                "new_path" => "something/new.md"
              }
            ]
          }
        end

        include_examples 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when changed file is `.gitlab/CODEOWNERS`' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => ".gitlab/CODEOWNERS",
              "new_path" => ".gitlab/CODEOWNERS"
            }
          ]
        }
      end

      it 'posts a review request comment for `.gitlab/CODEOWNERS`' do
        body = add_automation_suffix('processor/ceo_chief_of_staff_team_ops_notifier.rb') do
          <<~MARKDOWN.chomp
            #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
            Please tag `@gitlab-com/ceo-chief-of-staff-team` to review this MR as it modifies a `.gitlab/CODEOWNERS` that they maintain.

            /label ~"CoST" ~"handbook::content"
          MARKDOWN
        end

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when changed files are not `.gitlab/CODEOWNERS`' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "handbook/ceo/chief-of-staff-team/_index.md",
              "new_path" => "handbook/ceo/chief-of-staff-team/_index.md"
            }
          ]
        }
      end

      it 'posts a review request comment for not `.gitlab/CODEOWNERS`' do
        body = add_automation_suffix('processor/ceo_chief_of_staff_team_ops_notifier.rb') do
          <<~MARKDOWN.chomp
            #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
            Please tag `@gitlab-com/ceo-chief-of-staff-team` when you are ready for this MR to be reviewed as it modifies files related to the [CoST](https://handbook.gitlab.com/handbook/ceo/chief-of-staff-team/).

            /label ~"CoST" ~"handbook::content"
          MARKDOWN
        end

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when changed files are within LABELS_FOR_FILE' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => ".gitlab/CODEOWNERS",
              "new_path" => ".gitlab/CODEOWNERS"
            }
          ]
        }
      end

      it 'adds labels defined in LABELS_FOR_FILE' do
        body = add_automation_suffix('processor/ceo_chief_of_staff_team_ops_notifier.rb') do
          <<~MARKDOWN.chomp
            #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
            Please tag `@gitlab-com/ceo-chief-of-staff-team` to review this MR as it modifies a `.gitlab/CODEOWNERS` that they maintain.

            /label ~"CoST" ~"handbook::content"
          MARKDOWN
        end

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
