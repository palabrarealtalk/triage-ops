# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/automated_review_request_doc'

RSpec.describe Triage::AutomatedReviewRequestDoc do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        iid: merge_request_iid,
        project_web_url: 'https://gitlab.example/group/project',
        source_branch: 'feature'
      }
    end

    let(:merge_request_iid) { 300 }
    let(:added_label_names) { [Labels::WORKFLOW_READY_FOR_REVIEW_LABEL] }
  end

  let(:most_specific_approvers) { [] }
  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "app/models/misplaced/doc/user.md",
          "new_path" => "doc/user.md"
        }
      ]
    }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
    allow_any_instance_of(Triage::DocumentationCodeOwner).to receive(:most_specific_approvers).and_return(most_specific_approvers)
  end

  include_examples 'registers listeners', ["merge_request.update"]

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    context 'when MR has Technical Writing label' do
      let(:label_names) { ['Technical Writing'] }

      include_examples 'event is not applicable'
    end

    context 'when MR has tw::triaged label' do
      let(:label_names) { ['tw::triaged'] }

      include_examples 'event is not applicable'
    end

    context 'when MR has tw::doing label' do
      let(:label_names) { ['tw::doing'] }

      include_examples 'event is not applicable'
    end

    context 'when "workflow::ready for review" is not added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when merge request does not change docs' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              'old_path' => 'app/models/user.rb',
              'new_path' => 'app/models/user_renamed.rb'
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when docs review was already requested' do
      before do
        allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'process docs merge request' do
      let(:expected_approver) { most_specific_approvers.sample(random: Random.new(OpenSSL::Digest::SHA256.hexdigest(event.source_branch).to_i(16))) }

      it 'posts a comment mentioning technical writer and labeling the merge request' do
        body = add_automation_suffix('processor/community/automated_review_request_doc.rb') do
          <<~MARKDOWN.chomp
            #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
            Hi `@#{expected_approver}`! Please review this ~"documentation" merge request.
            /assign_reviewer @#{expected_approver}
            /label ~"documentation" ~"tw::triaged"
          MARKDOWN
        end

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when event is applicable' do
      let(:most_specific_approvers) { %w[tech_writer1 tech_writer2] }

      include_examples 'process docs merge request'
    end

    context 'when there are no matching approvers' do
      let(:most_specific_approvers) { [] }

      it 'only labels the merge request' do
        body = <<~MARKDOWN.chomp
          /label ~"documentation" ~"tw::triaged"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when merge request changes docs/' do
      let(:most_specific_approvers) { %w[tech_writer1 tech_writer2] }
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              'old_path' => 'docs/index.md',
              'new_path' => 'docs/index.md'
            }
          ]
        }
      end

      include_examples 'process docs merge request'
    end
  end
end
