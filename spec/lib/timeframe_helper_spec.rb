# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/timeframe_helper'

RSpec.describe TimeframeHelper do
  let(:start_date) { Date.new(2019, 10, 18) }
  let(:due_date) { Date.new(2019, 11, 17) }

  let(:resource_klass) do
    Struct.new(:issue) do
      include TimeframeHelper
    end
  end

  let(:timeframeable) do
    Struct.new(:start_date, :due_date).new(start_date, due_date)
  end

  subject { resource_klass.new }

  around do |example|
    Timecop.freeze(2019, 11, 1) { example.run }
  end

  describe '#days_until_due_date' do
    it 'returns the number of days until due date' do
      expect(subject.days_until_due_date(timeframeable)).to eq(16)
    end
  end

  describe '#days_since_start_date' do
    it 'returns the number of days since start date' do
      expect(subject.days_since_start_date(timeframeable)).to eq(14)
    end
  end
end
