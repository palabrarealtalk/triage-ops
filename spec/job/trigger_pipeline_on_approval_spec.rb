# frozen_string_literal: true
# frozien_string_literal: true

require 'spec_helper'

require_relative '../../triage/job/trigger_pipeline_on_approval_job'

RSpec.describe Triage::TriggerPipelineOnApprovalJob do
  describe '#perform' do
    let(:noteable_path) { 'projects/1/merge_requests/1' }

    it 'posts to noteable to trigger a pipeline and add a comment' do
      expect_api_requests do |requests|
        requests << stub_api_request(verb: :post, path: "#{noteable_path}/pipelines")

        subject.perform(noteable_path)
      end
    end
  end
end
