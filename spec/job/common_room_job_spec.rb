# frozen_string_literal: true

require 'gitlab'
require 'spec_helper'

require_relative '../../triage/job/common_room_job'
require_relative '../../triage/triage'
require_relative '../../triage/triage/event'
require_relative '../../triage/triage/common_room_api_client'

RSpec.describe Triage::CommonRoomJob do
  include_context 'with event', Triage::MergeRequestNoteEvent do
    let(:payload) do
      {
        'merge_request' => {
          'id' => noteable_id,
          'author_id' => noteable_author_id,
          'title' => noteable_title,
          'description' => noteable_description
        },
        'project' => {
          'path_with_namespace' => project_path_with_namespace
        }
      }
    end

    let(:event_attrs) do
      {
        id: 123,
        resource_author_id: noteable_author_id,
        description: "This feature would rock!\n\nKeep up the good work!",
        url: noteable_url,
        created_at: created_at,
        event_actor_id: note_author_id,
        from_gitlab_org?: from_gitlab_org,
        from_gitlab_com?: from_gitlab_com,
        wider_community_author?: wider_community_author
      }
    end
  end

  let(:note_author_id) { 8 }
  let(:noteable_id) { 7 }
  let(:noteable_author_id) { 4 }
  let(:created_at) { Date.new(2023, 10, 19) }
  let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  let(:noteable_title) { 'Fix the bug' }
  let(:noteable_description) { 'Details about fixing the bug.' }
  let(:wider_community_author) { true }
  let(:from_gitlab_com) { false }
  let(:from_gitlab_org) { true }
  let(:noteable_url) { 'http://gitlab.com/url' }

  let(:note_payload) { 'note' }
  let(:noteable_key) { 'issue_6' }
  let(:noteable_payload) { 'noteable' }
  let(:common_room_api_client) { instance_double(CommonRoomApiClient) }
  let(:gitlab_api_client) { instance_double(Gitlab::Client) }

  before do
    allow(CommonRoomApiClient).to receive(:new).and_return(common_room_api_client)
    allow(Triage).to receive(:api_client).and_return(gitlab_api_client)
  end

  describe '#perform' do
    before do
      allow(subject).to receive_messages(note: note_payload, noteable: noteable_payload)
    end

    context 'when note_author and noteable are cached' do
      before do
        allow(Triage.cache).to receive(:set?).with('user_8').and_return(true)
        allow(Triage.cache).to receive(:set?).with('merge_request_7').and_return(true)
      end

      it 'sends only the note to common room' do
        expect(common_room_api_client).to receive(:add_activity).with(note_payload)

        subject.perform(event)
      end
    end

    context 'when note_author, noteable and noteable_author are not cached' do
      before do
        allow(Triage.cache).to receive(:set?).with('user_8').and_return(nil)
        allow(Triage.cache).to receive(:set?).with('merge_request_7').and_return(nil)
        allow(Triage.cache).to receive(:set?).with('user_4').and_return(nil)
      end

      it 'sends the note_author, noteable, noteable_author and note to common room' do
        expect(common_room_api_client).to receive(:add_activity).with(user_payload(8))
        expect(common_room_api_client).to receive(:add_activity).with(user_payload(4))
        expect(common_room_api_client).to receive(:add_activity).with(note_payload)
        expect(common_room_api_client).to receive(:add_activity).with(noteable_payload)
        expect(gitlab_api_client).to receive(:user).with(8).and_return(user_stub(8))
        expect(gitlab_api_client).to receive(:user).with(4).and_return(user_stub(4))

        subject.perform(event)
      end
    end
  end

  describe '#truncate_description' do
    context 'when description is nil' do
      let(:description) { nil }

      it 'returns an empty string' do
        expect(subject.truncate_description(description)).to eq('')
      end
    end

    context 'when description > 90,000 chars' do
      let(:description) { 'a' * 90_001 }

      it 'returns the first 90,000 chars' do
        expect(subject.truncate_description(description)).to eq('a' * 90_000)
      end
    end

    context 'when less than 90,000 chars' do
      let(:description) { 'Hello!' }

      it 'returns the description untouched' do
        expect(subject.truncate_description(description)).to eq(description)
      end
    end
  end

  describe '#noteable' do
    include_context 'with event', Triage::IssueNoteEvent do
      let(:payload) do
        {
          'issue' => {
            'id' => noteable_id,
            'author_id' => noteable_author_id,
            'title' => noteable_title,
            'description' => noteable_description
          },
          'project' => {
            'path_with_namespace' => project_path_with_namespace
          }
        }
      end

      let(:event_attrs) do
        {
          resource_author_id: noteable_author_id,
          created_at: created_at,
          url: noteable_url
        }
      end
    end

    let(:expected_payload) do
      {
        activityTitle: { type: 'text', value: 'Fix the bug' },
        activityType: 'created_ticket',
        content: { type: 'markdown', value: 'Details about fixing the bug.' },
        id: '7',
        subSource: { name: 'gitlab-org/gitlab', type: 'byName' },
        tags: [],
        timestamp: Date.new(2023, 10, 19),
        url: noteable_url,
        user: { id: '4' }
      }
    end

    before do
      subject.instance_variable_set(:@event, event)
    end

    it 'builds expected payload' do
      expect(subject.noteable).to eq(expected_payload)
    end
  end

  describe '#note' do
    let(:expected_payload) do
      {
        activityTitle: { type: 'text', value: 'This feature would rock!' },
        activityType: 'commented_replied',
        content: { type: 'markdown', value: "This feature would rock!\n\nKeep up the good work!" },
        id: '123',
        subSource: { name: 'gitlab-org/gitlab', type: 'byName' },
        tags: [],
        timestamp: Date.new(2023, 10, 19),
        url: noteable_url,
        user: { id: '8' },
        parentActivity: { activityType: 'made_merge_request', id: '7' }
      }
    end

    before do
      subject.instance_variable_set(:@event, event)
    end

    it 'builds expected payload' do
      expect(subject.note).to eq(expected_payload)
    end
  end

  def user_stub(id)
    hash = {
      name: "User #{id}",
      avatar_url: "user_#{id}_avatar_url",
      bio: "User #{id} bio.",
      web_url: "user_#{id}_web_url",
      public_email: "user#{id}@example.com",
      linkedin: "user#{id}",
      twitter: "@user#{id}",
      organization: "User #{id} Org",
      job_title: "User #{id} job",
      location: "User #{id} location",
      created_at: Date.new(2023, 10, id)
    }
    Gitlab::ObjectifiedHash.new(hash)
  end

  def user_payload(id)
    {
      user: {
        id: id.to_s,
        fullName: "User #{id}",
        avatarUrl: "user_#{id}_avatar_url",
        bio: "User #{id} bio.",
        externalProfiles: [{ url: "user_#{id}_web_url" }],
        email: "user#{id}@example.com",
        linkedin: { type: 'handle', value: "user#{id}" },
        twitter: { type: 'handle', value: "@user#{id}" },
        discord: nil, # Need to call the Discord API to get this!!!
        companyName: "User #{id} Org",
        titleAtCompany: "User #{id} job",
        rawLocation: "User #{id} location"
      },
      id: id.to_s,
      activityType: 'joined',
      timestamp: Date.new(2023, 10, id)
    }
  end
end
