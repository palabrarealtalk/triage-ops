# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/rack/error_handler'

RSpec.describe Triage::Rack::ErrorHandler do
  let(:fake_app) { double }
  let(:app)      { described_class.new(fake_app) }
  let(:logger)   { double(error: true) }
  let(:env)      { { Rack::RACK_LOGGER => logger } }
  let(:response) { app.call(env) }
  let(:status)   { response[0] }
  let(:body)     { response[2][0] }

  before do
    allow(app).to receive(:puts)
  end

  context 'when an error is thrown' do
    let(:error) { StandardError.new('runtime error') }

    before do
      allow(fake_app).to receive(:call).with(env).and_raise(error)
    end

    it 'returns a 500 error and tags Sentry error with event type' do
      expect(Raven).to receive(:capture_exception).with(error)

      expect(status).to eq(500)
      expect(body).to eq(JSON.dump(status: :error, error: "StandardError", message: "runtime error"))
    end
  end

  context 'when no error is thrown' do
    it 'calls the rest of the middleware stack' do
      expect(fake_app).to receive(:call).with(env)

      response
    end
  end
end
