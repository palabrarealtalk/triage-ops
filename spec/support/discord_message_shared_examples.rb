# frozen_string_literal: true

RSpec.shared_context 'with discord posting context' do |webhook_name|
  let(:discord_messenger_stub) { instance_double(DiscordMessenger) }
  let(:discord_webhook_path) { 'test_test' }

  before do
    stub_env(webhook_name => discord_webhook_path)
    allow(DiscordMessenger).to receive(:new).and_return(discord_messenger_stub)
  end

  shared_examples 'discord message posting' do
    it 'posts a discord message' do
      subject.process

      expect(discord_messenger_stub).to have_received(:send_message).exactly(1).times.with(message_body)
    end
  end

  shared_examples 'no discord message posting' do
    it 'does not post a discord message' do
      subject.process

      expect(discord_messenger_stub).not_to have_received(:send_message)
    end
  end
end

RSpec.shared_examples 'instantiates discord messenger' do
  context 'with discord webhook path present' do
    let(:discord_webhook_path) { 'test_test' }

    it 'instantiates discord messenger with the provided webhook path' do
      expect(DiscordMessenger).to receive(:new).with(discord_webhook_path)

      described_class.new(event)
    end
  end

  context 'with discord webhook path not present' do
    let(:discord_webhook_path) { nil }

    it 'instantiates discord messenger with nil webhook path' do
      expect(DiscordMessenger).to receive(:new).with(discord_webhook_path)

      described_class.new(event)
    end
  end
end
