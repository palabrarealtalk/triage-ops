# AppSec approval process

[[_TOC_]]

## Why?

We require AppSec approval for some specific merge requests. The high level
goal is that we want to make sure that all the merge requests meeting the
conditions should be approved by AppSec for the latest revision.

We also don't want to hinder development velocity so we try to ask AppSec
review after first approval, assuming that it's in a relatively ready state.

Whenever there's any new changes pushed, we'll revoke the approval and ask
AppSec to follow up.

See [Security review workflow](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-security-review-process.html#security-review-workflow-for-jihu-contributions) for more details.

## Example workflow

* After first approval: Add a new unresolved discussion to ask AppSec review.
* After AppSec approved: Add a new comment in the discussion about who
  approved and for which revision, resolving the discussion.
* When new changes are pushed: Add a new comment in the discussion to ask
  who originally approved, review again. Unresolve the discussion.
* After AppSec approved again: Add a new comment in the discussion about who
  approved and for which revision, resolving the discussion.

## Involved processors

* `PingAppSecOnApproval` is responsible to ask for AppSec review, creating the
  discussion thread after any first approval.
* `ApprovedByAppSec` is responsible to add a comment for who approved on
  behave of AppSec and for which revision.
* `RevokeAppSecApproval` is responsible to remove the approval and ask the
  previous approver to review again whenever there are new changes pushed.
