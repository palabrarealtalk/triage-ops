# Degrade health status according to some basic rules. See https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/1074

resource_rules:
  issues:
    rules:
      - name: Set on track in the first 7 days of the milestone
        conditions:
          state: opened
          labels:
            - Deliverable
          forbidden_labels:
            - security
            - Untrack Health Status
          ruby: |
            VersionedMilestone.new(self).current?(milestone) &&
              days_since_start_date(milestone) < 7 &&
              resource[:health_status] != 'on_track' &&
              !has_unique_comment?("triage-ops set-health-status-on-track-#{today.year}-#{today.month}")
        actions:
          comment: |
            <!-- triage-ops set-health-status-on-track-#{today.year}-#{today.month} -->
            Setting health status to `on track` as the milestone has just begun.

            Issue participants are welcome to override this by setting the health status to another value.

            /health_status on_track
      - name: Set needs attention if there is no assignee within 11 days of end-of-milestone
        conditions:
          state: opened
          labels:
            - Deliverable
          forbidden_labels:
            - security
            - Untrack Health Status
          ruby: |
            VersionedMilestone.new(self).current?(milestone) &&
              days_until_due_date(milestone) <= 11 &&
              days_until_due_date(milestone) > 3 &&
              resource[:assignees].empty? &&
              !%w(at_risk needs_attention).include?(resource[:health_status]) &&
              !has_unique_comment?("triage-ops set-health-status-needs-attention-no-assignee-#{today.year}-#{today.month}")
        actions:
          comment: |
            <!-- triage-ops set-health-status-needs-attention-no-assignee-#{today.year}-#{today.month} -->
            This issue is scheduled for completion in this milestone but doesn't
            have an assignee. Changing health status to 'needs attention'.

            Issue participants are welcome to override this by setting the health status to another value.

            /health_status needs_attention
      - name: Set needs attention if not in development within 7 days of end-of-milestone
        conditions:
          state: opened
          labels:
            - Deliverable
          forbidden_labels:
            - security
            - Untrack Health Status
            - workflow::in dev
            - workflow::ready for review
            - workflow::in review
            - workflow::verification
            - workflow::production
          ruby: |
            VersionedMilestone.new(self).current?(milestone) &&
              days_until_due_date(milestone) <= 7 &&
              days_until_due_date(milestone) > 3 &&
              resource[:assignees].any? &&
              !%w(at_risk needs_attention).include?(resource[:health_status]) &&
              !has_unique_comment?("triage-ops set-health-status-needs-attention-workflow-state-#{today.year}-#{today.month}")
        actions:
          comment: |
            <!-- triage-ops set-health-status-needs-attention-workflow-state-#{today.year}-#{today.month} -->
            This issue is scheduled for completion in this milestone but is not yet in development. Changing health status to 'needs attention'.

            Issue participants are welcome to override this by setting the health status to another value.

            /health_status needs_attention
      - name: Set at risk if there is no assignee within 3 days of end-of-milestone
        conditions:
          state: opened
          labels:
            - Deliverable
          forbidden_labels:
            - security
            - Untrack Health Status
          ruby: |
            VersionedMilestone.new(self).current?(milestone) &&
              days_until_due_date(milestone) <= 3 &&
              resource[:assignees].empty? &&
              resource[:health_status] != 'at_risk' &&
              !has_unique_comment?("triage-ops set-health-status-at-risk-no-assignee-#{today.year}-#{today.month}")
        actions:
          comment: |
            <!-- triage-ops set-health-status-at-risk-no-assignee-#{today.year}-#{today.month} -->
            This issue is scheduled for completion in this milestone but doesn't
            have an assignee. Changing health status to 'at risk'.

            Issue participants are welcome to override this by setting the health status to another value.

            /health_status at_risk
      - name: Set at risk if not in review within 3 days of end-of-milestone
        conditions:
          state: opened
          labels:
            - Deliverable
          forbidden_labels:
            - workflow::in review
            - workflow::verification
            - workflow::production
            - security
            - Untrack Health Status
          ruby: |
            VersionedMilestone.new(self).current?(milestone) &&
              days_until_due_date(milestone) <= 3 &&
              resource[:assignees].any? &&
              resource[:health_status] != 'at_risk' &&
              !has_unique_comment?("triage-ops set-health-status-at-risk-workflow-state-#{today.year}-#{today.month}")
        actions:
          comment: |
            <!-- triage-ops set-health-status-at-risk-workflow-state-#{today.year}-#{today.month} -->
            This issue is scheduled for completion in this milestone but is in
            an early development stage. Changing health status to 'at risk'.

            Issue participants are welcome to override this by setting the health status to another value.

            /health_status at_risk
